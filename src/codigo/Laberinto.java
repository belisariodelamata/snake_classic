
package codigo;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;
import javax.swing.Timer;


//Esta lista de nodos la uso para crear el codigo para el laberinto
/**
 * @author BS
 */
public class Laberinto {
    private JPanel area;//contenedor del juego
    private Image tmp;//temporal para el doble buffer
    private Graphics bsim; //con esta es que dibujaré directamente
    private int cuadrosx,cuadrosy;//cantidad de los cuadros horizontal y vertical
    private long tcuadrox,tcuadroy;//tamaño de los cuadros horizontal y vertical
    private Image prox;//imagen que se usara el proximo dibujo
    private Image prox2;//utilizada para imagenes secundarias
    
    private Game world=null;//la variable relacional mundo que involucra valores para el
    //manejo de los laberintos y la culebra

    private boolean[][] cuadrocupado;//guardará si una coordenada está ocupada ya sea por un bloque, por la culebra
    //o por las vitaminas
    private int espaciosNoOcupados;//guardará cuantos espacios están Vacios;

    private Timer movSnake, cambioVit;

    private BodySnake body=null;

    private boolean dibujardirecto=true;

    private boolean borrarAntesDeDibujar=false;
    private boolean borrarMemoria=true;

  
    private int idFondo=0,idBloque=0;

    private NodoCuerpo origen=new NodoCuerpo();
    private NodoCuerpo destino=new NodoCuerpo();

    public void setSnake(BodySnake body){
        this.body=body;
        this.setWorld(body.getWorld());
    }
    public void dibujarCola(){
        borrarAntesDeDibujar=true;
        prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(getBody().getCola().dir+12)+"-"+this.world.getMovCola()+".png")).getImage();
        dibujar(getBody().getCola().x, getBody().getCola().y);
    }
    public void dibujarCabeza(){
        prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(getBody().getCabeza().dir+16)+"-"+getWorld().getMovCabeza()+".png")).getImage();
        dibujar(getBody().getCabeza().x, getBody().getCabeza().y);
    }
    public void dibujarCabeza(int idPosicion){
        prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(getBody().getCabeza().dir+idPosicion)+".png")).getImage();
        dibujar(getBody().getCabeza().x, getBody().getCabeza().y);
    }

    public void dibujarSnake(){
        
        NodoCuerpo nc=this.getBody().getCabeza();
        dibujardirecto=false;
        while(nc!=null){

        ///////////////////////
        secuenciaMov();
        /////////////////////

            if (nc==this.getBody().getCabeza()){
                dibujarCabeza();
            }else if (nc.getSig()==null){
                dibujarCola();
            }else{
                prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(2)+"-"+this.world.getMovCuerpo()+".png")).getImage();
//                prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(nc.dir)+".png")).getImage();
                    dibujar(nc.x, nc.y);
            }
            nc=nc.getSig();
        }
        repaint();
        dibujardirecto=true;
    }

    public Laberinto(JPanel area, int cuadrosx, int cuadrosy, BodySnake body) {
        this.body=body;
        this.cuadrosx=cuadrosx;this.cuadrosy=cuadrosy;

        cuadrocupado=new boolean[cuadrosx][cuadrosy];espaciosNoOcupados=cuadrosx*cuadrosy;
        tcuadrox=(long) area.getWidth()/cuadrosx;
        tcuadroy=(long) area.getHeight()/cuadrosy;
        this.area = area;
        this.bsim=area.getGraphics();
        this.tmp=area.createImage(area.getWidth(),area.getHeight());

        idFondo=(int)(Math.random()*4+1);
        idBloque=(int)(Math.random()*4+1);
    }
    public int getScaleHeight(){
        return cuadrosy;
    }   
    public int getScaleWidth(){
        return cuadrosx;
    }
    public void repaint(){
        area.getGraphics().drawImage(tmp, 0, 0, null);
    }

    public void establecerFondo(){
        for (int i = 0; i < this.getScaleWidth(); i++) {
            for (int j = 0; j < this.getScaleHeight(); j++) {
                borrarMemoria=true;
                borrar(i, j);
            }
        }
    }
    public void dibujarBs(){
        establecerFondo();
        //prox=new javax.swing.ImageIcon(getClass().getResource("/extras/Bloque.png")).getImage();
        int x;
        
        dibujardirecto=false;
        drawBloackR(0, 0);drawBloackR(1, 0);drawBloackR(2, 0);
        for (x=5;x<=8;x++)drawBloackR(x,0);drawBloackR(10, 0);
        for (x=17; x<=21;x++)drawBloackR(21,0);drawBloackR(0, 1);
        drawBloackR(3, 1);drawBloackR(5, 1);drawBloackR(10, 1);drawBloackR(21,1);drawBloackR(0, 2);drawBloackR(1, 2);
        drawBloackR(2, 2);drawBloackR(5, 2);drawBloackR(6, 2);drawBloackR(7, 2);drawBloackR(10, 2);drawBloackR(17, 2);
        drawBloackR(18, 2);drawBloackR(19, 2);drawBloackR(21,2);drawBloackR(0, 3);drawBloackR(3, 3);drawBloackR(5, 3);
        drawBloackR(10, 3);drawBloackR(21,3);drawBloackR(0, 4);drawBloackR(1, 4);drawBloackR(2, 4);
        for (x=5;x<=8;x++)drawBloackR(x,4);
        for (x=10;x<=13;x++)drawBloackR(x,4);drawBloackR(21,4);
        drawBloackR(21,5);drawBloackR(1, 6);drawBloackR(2, 6);drawBloackR(3, 6);drawBloackR(6, 6);drawBloackR(7, 6);
        for (x=10;x<=13;x++)drawBloackR(x,6);
        drawBloackR(15, 6);drawBloackR(16, 6);drawBloackR(17, 6);drawBloackR(21,6);drawBloackR(0, 7);drawBloackR(5, 7);
        drawBloackR(8, 7);drawBloackR(10, 7);drawBloackR(16, 7);drawBloackR(21,7);drawBloackR(1, 8);drawBloackR(2, 8);
        drawBloackR(5, 8);drawBloackR(8, 8);drawBloackR(10, 8);drawBloackR(11, 8);drawBloackR(12, 8);drawBloackR(16, 8);
        drawBloackR(21,8);drawBloackR(3, 9);drawBloackR(5, 9);drawBloackR(8, 9);drawBloackR(10, 9);drawBloackR(16, 9);
        drawBloackR(21,9);drawBloackR(0, 10);drawBloackR(1, 10);drawBloackR(2, 10);drawBloackR(6, 10);drawBloackR(7, 10);
        drawBloackR(10, 10);drawBloackR(16, 10);drawBloackR(21,10);
        for (x=18; x<=21;x++)drawBloackR(21,11);
        repaint();
        
        dibujardirecto=true;
    }
    public void dibujar( int x, int y){
       if (borrarAntesDeDibujar){//necesario para que no quede la imagen debajo, efecto dado por la transparencia
           this.borrarMemoria=false;

           borrar(x, y);
           //bsim.clearRect((int)tcuadrox*x, (int)tcuadroy*y,(int)tcuadrox, (int)tcuadroy);
           //tmp.getGraphics().clearRect((int)tcuadrox*x, (int)tcuadroy*y,(int)tcuadrox, (int)tcuadroy);
           this.borrarMemoria=true;
       }
       if (dibujardirecto && bsim!=null) {
           bsim.drawImage(prox, (int)(tcuadrox*x), (int)(tcuadroy*y), (int)(tcuadrox), (int)tcuadroy, null);
       }
        tmp.getGraphics().drawImage(prox, (int)(tcuadrox*x), (int)(tcuadroy*y), (int)(tcuadrox), (int)tcuadroy, null);
       if (getCuadrocupado()[x][y]==false){//si el cuadro no estaba dibujado
           cuadrocupado[x][y]=true;           //establecemos que hicimos un dibujo en el
           espaciosNoOcupados--;           //vamos relacionando internamente cuantos cuadros faltan para que el cuadro se llene
       }
    }
    public void dibujar(NodoCuerpo nc){
        dibujar(nc.x, nc.y);
    }
    public void secuenciaMov(){
        world.movCabeza++;
        world.movCuerpo++;
        if (world.comioVit==false){
            world.movCola++;
            if (world.movCola==2)world.movCola=0;
            System.out.println(body.getCabeza().dir);
        }
        if (world.movCabeza >= 2) {
            world.movCabeza = 0;
            world.movCuerpo=0;
        }
    }
    public void borrarReal(int x,int y){
        borrar((int)(x/this.tcuadrox), (int)(y/this.tcuadroy));
    }
     public void borrar(int x,int y){

           prox2=new javax.swing.ImageIcon(getClass().getResource("/extras/f"+ this.idFondo+".png")).getImage();
           bsim.drawImage(prox2, (int)(tcuadrox*x), (int)(tcuadroy*y), (int)(tcuadrox), (int)tcuadroy, null);
           tmp.getGraphics().drawImage(prox2, (int)(tcuadrox*x), (int)(tcuadroy*y), (int)(tcuadrox), (int)tcuadroy, null);
//        bsim.clearRect((int)tcuadrox*x, (int)tcuadroy*y, (int)tcuadrox, (int)tcuadroy);
  //      tmp.getGraphics().clearRect((int)tcuadrox*x, (int)tcuadroy*y, (int)tcuadrox, (int)tcuadroy);
       if (getCuadrocupado()[x][y]==true && this.borrarMemoria){//si el cuadro no estaba dibujado
           cuadrocupado[x][y]=false;           //establecemos que hicimos un dibujo en el
           espaciosNoOcupados++;           //vamos relacionando internamente cuantos cuadros faltan para que el cuadro se llene
       }

    }
    public void borrar(NodoCuerpo nc){
        borrar(nc.x, nc.y);
    }
    public void quitarColaEliminada(){
        borrar(getBody().ultCola().x, getBody().ultCola().y);
    }

    public void mostrarBody(byte valor, byte modo){
        NodoCuerpo nc=getBody().getCabeza().getSig();
        if (valor %2==0 ){
            if (modo==0){//si va a mostrar el cuerpo
                prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(2)+"-"+this.world.movCuerpo+".png")).getImage();
            }else prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(2+modo)+".png")).getImage();
        }else{
            if (modo==0){//si va a mostrar el cuerpo
                prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(1)+"-"+this.world.movCuerpo+".png")).getImage();
            }else prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(1+modo)+".png")).getImage();
        }
        dibujar(nc);
    }
    public void mostrarDireccion(){
        try{
        if (getWorld().mostrarLlenura && getWorld().mostrarDireccion==false){
            borrarAntesDeDibujar=true;
            mostrarBody((byte)getBody().getCabeza().getSig().dir,(byte) 6);
            world.mostrarLlenura=false;
        }else if(getWorld().mostrarLlenura && getWorld().mostrarDireccion==true){
            mostrarCruce(6, getBody().getCabeza().getSig().dir, getBody().getCabeza().getSig().getSig().dir);
            world.mostrarLlenura=false;
            world.mostrarDireccion=false;
        }else if(getWorld().mostrarDireccion){     
            mostrarCruce(0, getBody().getCabeza().getSig().dir, getBody().getCabeza().getSig().getSig().dir);
            world.mostrarDireccion=false;
        }else{
            mostrarBody((byte)getBody().getCabeza().getSig().dir,(byte) 0);
        }
        }catch (Exception e){
            
        }
    }
    public void mostrarCruce(int modo, int dir1,int dir2){
        NodoCuerpo nc=getBody().getCabeza().getSig();
        borrarAntesDeDibujar=true;
        switch(dir1){
            case 2://derecha
                switch(dir2){
                    case 3://arriba
                    prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(3+modo)+".png")).getImage();
                    dibujar(nc.x, nc.y);break;
                    case 1://abajo
                    prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(6+modo)+".png")).getImage();
                    dibujar(nc.x, nc.y);break;
                }break;
            case 1://abajo
                switch(dir2){
                    case 2://derecha
                    prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(4+modo)+".png")).getImage();
                    dibujar(nc.x, nc.y);break;
                    case 4://izquierda
                    prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(3+modo)+".png")).getImage();
                    dibujar(nc.x, nc.y);break;
                }break;
            case 4://izquierda
                switch(dir2){
                    case 3://arriba
                    prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(4+modo)+".png")).getImage();
                    dibujar(nc.x, nc.y);break;
                    case 1://abajo
                    prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(5+modo)+".png")).getImage();
                    dibujar(nc.x, nc.y);break;
                }break;
            case 3://arriba
                switch(dir2){
                    case 2://derecha
                    prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(5+modo)+".png")).getImage();
                    dibujar(nc.x, nc.y);break;
                    case 4://izquierda
                    prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/"+(6+modo)+".png")).getImage();
                    dibujar(nc.x, nc.y);break;
                }break;
        }
        borrarAntesDeDibujar=false;
    }
    public void mostrarVit(NodoCuerpo vit){
        prox=new javax.swing.ImageIcon(getClass().getResource("/bodySnake/extras/r"+world.moviVit+".png")).getImage();
        dibujar(world.Vit);
    }
    public void randomVit_o_bono(byte tipo){
        movSnake.stop();
    //    cambioVit.stop();
        //tipo 1 vitamina
        //tipo 2 bono
        if (this.getEspaciosNoOcupados()==1){
            if (getWorld().salioBono){
                borrar(getWorld().Bono.x, getWorld().Bono.y);
                world.Bono.x=-1;world.Bono.y=-1;
                world.salioBono=false;
                //quitar etiqueta de timer
            }
        }
        int x,y;
        do{
            x=(int)(Math.random()*cuadrosx);
            y=(int)(Math.random()*cuadrosy);
        }while(getCuadrocupado(x, y)==true);//repite hasta que la vitamina aparezca en uno no ocupado

        if (tipo==1){
            prox=new javax.swing.ImageIcon(getClass().getResource("/extras/V1.png")).getImage();
            world.Vit.x=x;world.Vit.y=y;
            dibujar(x, y);
        }else if (tipo==2){
            prox=new javax.swing.ImageIcon(getClass().getResource("/extras/Bono.png")).getImage();
            //NodoCuerpo nc=world.Vit.clone();
            //nc.dir=2;
            //siguientepos(nc);
            //x=nc.x;y=nc.y;
            ////
            world.Bono.x=x;world.Bono.y=y;
            dibujar(x, y);
        }

        movSnake.start();
//        cambioVit.start();
    }
    public void dibujarEstrellada(){
            borrarAntesDeDibujar=false;
            //di
            dibujarCabeza();
            //prox=new javax.swing.ImageIcon(getClass().getResource("/extras/E.png")).getImage();
            //dibujar(getBody().getCabeza());
    }
    public boolean getCuadroocupadoNoComida(NodoCuerpo nc){
        if (getCuadrocupado()[nc.x][nc.y]){
            if (nc.igualPosicion(getWorld().Bono) || nc.igualPosicion(getWorld().Vit)){
                return false;
            }else return true;

        }else{
            return false;
        }
    }
    public boolean getCuadrocupado(NodoCuerpo nc){
        return getCuadrocupado()[nc.x][nc.y];
    }    public boolean getCuadrocupado(int x,int y) {
        return getCuadrocupado()[x][y];
    }    public int getEspaciosNoOcupados() {
        return espaciosNoOcupados;
    }
    public void setMovSnake(Timer movSnake) {
        this.movSnake = movSnake;
    }
    public void setCambioVit(Timer cambioVit) {
        this.cambioVit = cambioVit;
    }
    public void golpe(){
        world.estaJugando=false;
        movSnake.stop();
        dibujarEstrellada();
    }
    public void borrarTodo(){
        //bsim=area.getGraphics();
        cuadrocupado=new boolean[cuadrosx][cuadrosy];espaciosNoOcupados=cuadrosx*cuadrosy;
        bsim.clearRect(0, 0, area.getWidth(), area.getHeight());
        area.getGraphics().clearRect(0, 0, area.getWidth(), area.getHeight());
        System.out.println(area.getWidth()+" " + area.getHeight());
    }
    public void dibujarLaberinto(int num){
        borrarTodo();
        establecerFondo();
        prox=new javax.swing.ImageIcon(getClass().getResource("/extras/b"+this.idBloque+".png")).getImage();
        dibujardirecto=false;
        int x;
        switch(num){
            case 1:


                /*for (x=0;x<=10;x++)drawBloackR(x,0);
                for (x=12;x<=16;x++)drawBloackR(x,0);
                for (x=18; x<=21;x++)drawBloackR( x,0);drawBloackR(3, 1);drawBloackR(4, 1);drawBloackR(5, 1);drawBloackR(18, 1);drawBloackR(19, 1);
                drawBloackR(0, 2);drawBloackR(1, 2);drawBloackR(4, 2);drawBloackR(5, 2);drawBloackR(7, 2);drawBloackR(8, 2);
                for (x=10;x<=19;x++)drawBloackR(x,2);
                drawBloackR(21,2);drawBloackR(0, 3);drawBloackR(1, 3);drawBloackR(2, 3);drawBloackR(5, 3);drawBloackR(8, 3);
                drawBloackR(11, 3);drawBloackR(12, 3);drawBloackR(13, 3);drawBloackR(21,3);
                for (x=0;x<=3;x++)drawBloackR(x,4);drawBloackR(6, 4);drawBloackR(9, 4);
                drawBloackR(12, 4);drawBloackR(13, 4);
                for (x=15;x<=19;x++)drawBloackR(x,4);drawBloackR(21,4);drawBloackR(0, 5);drawBloackR(4, 5);drawBloackR(7, 5);
                drawBloackR(10, 5);
                for (x=15;x<=19;x++)drawBloackR(x,5);drawBloackR(0, 6);drawBloackR(2, 6);drawBloackR(5, 6);drawBloackR(7, 6);drawBloackR(8, 6);
                for (x=11;x<=16;x++)drawBloackR(x,6);
                drawBloackR(19, 6);drawBloackR(0, 7);drawBloackR(2, 7);drawBloackR(7, 7);drawBloackR(8, 7);drawBloackR(9, 7);
                drawBloackR(12, 7);drawBloackR(17, 7);drawBloackR(19, 7);drawBloackR(0, 8);drawBloackR(2, 8);drawBloackR(4, 8);
                drawBloackR(5, 8);drawBloackR(10, 8);drawBloackR(13, 8);drawBloackR(16, 8);drawBloackR(20, 8);drawBloackR(0, 9);
                drawBloackR(2, 9);
                for (x=4;x<=8;x++)drawBloackR(x,9);drawBloackR(11, 9);drawBloackR(14, 9);drawBloackR(15, 9);drawBloackR(16, 9);drawBloackR(18, 9);
                drawBloackR(21,9);
                for (x=5;x<=8;x++)drawBloackR(x,10);drawBloackR(12, 10);drawBloackR(18, 10);
                for (x=0;x<=3;x++)drawBloackR(x,11);drawBloackR(10, 11);
                for (x=12;x<=16;x++)drawBloackR(x,11);
                for (x=18; x<=21;x++)drawBloackR( x,11);*/

                /*drawBloackR(2,0);drawBloackR(11,0);drawBloackR(14,0);drawBloackR(15,0);drawBloackR(2,1);drawBloackR(7,1);
                drawBloackR(13,1);drawBloackR(14,1);drawBloackR(2,2);for (x=9; x<=14;x++)
                drawBloackR (x,2);drawBloackR(16,2);drawBloackR(18,2);drawBloackR(20,2);
                drawBloackR(2,3);drawBloackR(7,3);drawBloackR(10,3);drawBloackR(13,3);drawBloackR(19,3);drawBloackR(2,4);
                drawBloackR(6,4);drawBloackR(12,4);drawBloackR(13,4);drawBloackR(15,4);drawBloackR(17,4);drawBloackR(18,4);
                drawBloackR(2,5);drawBloackR(5,5);drawBloackR(9,5);for (x=13; x<=17;x++)
                drawBloackR (x,5);drawBloackR(19,5);drawBloackR(20,5);drawBloackR(21,5);
                drawBloackR(2,6);drawBloackR(4,6);drawBloackR(8,6);drawBloackR(9,6);drawBloackR(11,6);drawBloackR(13,6);
                drawBloackR(2,7);drawBloackR(3,7);drawBloackR(6,7);drawBloackR(9,7);drawBloackR(13,7);drawBloackR(16,7);
                drawBloackR(17,7);drawBloackR(19,7);drawBloackR(2,8);for (x=6; x<=9;x++)
                drawBloackR (x,8);drawBloackR(11,8);drawBloackR(12,8);drawBloackR(13,8);
                drawBloackR(15,8);drawBloackR(18,8);drawBloackR(1,9);drawBloackR(3,9);drawBloackR(6,9);drawBloackR(11,9);
                drawBloackR(13,9);drawBloackR(18,9);drawBloackR(0,10);for (x=2; x<=6;x++)
                drawBloackR (x,10);drawBloackR(13,10);drawBloackR(18,10);drawBloackR(19,10);
                drawBloackR(20,10);drawBloackR(2,11);drawBloackR(3,11);for (x=6; x<=18;x++)
                drawBloackR (x,11);drawBloackR(21,11);*/
                for (x=0; x<=21;x++)drawBloackR(x,0);
                drawBloackR(0, 1);drawBloackR(21,1);drawBloackR(0, 2);drawBloackR(21,2);drawBloackR(0, 3);
                drawBloackR(21,3);drawBloackR(0, 4);drawBloackR(21,4);drawBloackR(0, 5);drawBloackR(21,5);drawBloackR(0, 6);
                drawBloackR(21,6);drawBloackR(0, 7);drawBloackR(21,7);drawBloackR(0, 8);drawBloackR(21,8);drawBloackR(0, 9);
                drawBloackR(21,9);drawBloackR(0, 10);drawBloackR(21,10);
                for (x=0; x<=21;x++)drawBloackR( x,11);

                /*for (x=0; x<=47;x++)
                drawBloackR (x,0);drawBloackR(0,1);drawBloackR(3,1);drawBloackR(8,1);drawBloackR(18,1);drawBloackR(29,1);drawBloackR(33,1);
                drawBloackR(36,1);drawBloackR(49,1);drawBloackR(0,2);drawBloackR(3,2);drawBloackR(13,2);drawBloackR(15,2);
                drawBloackR(17,2);drawBloackR(21,2);for (x=23; x<=26;x++)
                drawBloackR (x,2);drawBloackR(30,2);drawBloackR(37,2);drawBloackR(43,2);drawBloackR(46,2);
                drawBloackR(49,2);drawBloackR(0,3);drawBloackR(1,3);drawBloackR(11,3);drawBloackR(16,3);drawBloackR(21,3);
                drawBloackR(29,3);drawBloackR(31,3);drawBloackR(33,3);drawBloackR(36,3);drawBloackR(39,3);drawBloackR(42,3);
                drawBloackR(44,3);drawBloackR(49,3);drawBloackR(0,4);drawBloackR(6,4);drawBloackR(7,4);drawBloackR(10,4);
                drawBloackR(15,4);drawBloackR(21,4);drawBloackR(23,4);drawBloackR(26,4);drawBloackR(28,4);drawBloackR(37,4);
                drawBloackR(38,4);drawBloackR(41,4);drawBloackR(42,4);drawBloackR(44,4);drawBloackR(46,4);drawBloackR(49,4);
                drawBloackR(0,5);drawBloackR(3,5);drawBloackR(15,5);drawBloackR(20,5);drawBloackR(21,5);drawBloackR(24,5);
                drawBloackR(26,5);drawBloackR(29,5);drawBloackR(33,5);drawBloackR(41,5);drawBloackR(46,5);drawBloackR(49,5);
                drawBloackR(0,6);drawBloackR(3,6);drawBloackR(14,6);drawBloackR(17,6);drawBloackR(21,6);drawBloackR(23,6);
                drawBloackR(26,6);drawBloackR(29,6);drawBloackR(32,6);drawBloackR(33,6);drawBloackR(36,6);drawBloackR(37,6);
                drawBloackR(41,6);drawBloackR(44,6);drawBloackR(46,6);drawBloackR(49,6);drawBloackR(0,7);drawBloackR(7,7);
                drawBloackR(13,7);drawBloackR(16,7);drawBloackR(21,7);drawBloackR(25,7);drawBloackR(26,7);drawBloackR(28,7);
                drawBloackR(34,7);drawBloackR(36,7);drawBloackR(38,7);drawBloackR(46,7);drawBloackR(48,7);drawBloackR(49,7);
                drawBloackR(0,8);drawBloackR(1,8);drawBloackR(9,8);drawBloackR(12,8);drawBloackR(19,8);drawBloackR(21,8);
                drawBloackR(22,8);drawBloackR(29,8);drawBloackR(32,8);drawBloackR(37,8);drawBloackR(40,8);drawBloackR(41,8);
                drawBloackR(43,8);drawBloackR(46,8);drawBloackR(48,8);drawBloackR(49,8);drawBloackR(0,9);drawBloackR(4,9);
                drawBloackR(11,9);drawBloackR(16,9);drawBloackR(21,9);drawBloackR(23,9);drawBloackR(26,9);drawBloackR(30,9);
                drawBloackR(34,9);drawBloackR(43,9);drawBloackR(46,9);drawBloackR(48,9);drawBloackR(49,9);drawBloackR(0,10);
                drawBloackR(3,10);drawBloackR(5,10);drawBloackR(10,10);drawBloackR(21,10);drawBloackR(24,10);drawBloackR(26,10);
                drawBloackR(27,10);drawBloackR(31,10);drawBloackR(34,10);drawBloackR(36,10);drawBloackR(39,10);drawBloackR(46,10);
                drawBloackR(49,10);for (x=0; x<=21;x++)
                drawBloackR (x,11);drawBloackR(26,11);drawBloackR(34,11);drawBloackR(36,11);drawBloackR(43,11);drawBloackR(46,11);
                drawBloackR(49,11);drawBloackR(0,12);drawBloackR(8,12);drawBloackR(18,12);drawBloackR(20,12);drawBloackR(39,12);
                drawBloackR(41,12);drawBloackR(42,12);drawBloackR(44,12);drawBloackR(46,12);drawBloackR(48,12);drawBloackR(49,12);
                drawBloackR(0,13);drawBloackR(2,13);drawBloackR(3,13);drawBloackR(7,13);drawBloackR(8,13);drawBloackR(15,13);
                drawBloackR(17,13);drawBloackR(21,13);drawBloackR(24,13);drawBloackR(26,13);drawBloackR(27,13);drawBloackR(29,13);
                drawBloackR(32,13);drawBloackR(33,13);drawBloackR(34,13);drawBloackR(36,13);drawBloackR(40,13);drawBloackR(45,13);
                drawBloackR(46,13);drawBloackR(49,13);drawBloackR(0,14);drawBloackR(6,14);drawBloackR(15,14);drawBloackR(16,14);
                drawBloackR(22,14);drawBloackR(27,14);drawBloackR(29,14);drawBloackR(30,14);drawBloackR(36,14);drawBloackR(38,14);
                drawBloackR(39,14);drawBloackR(43,14);drawBloackR(45,14);drawBloackR(46,14);drawBloackR(49,14);drawBloackR(0,15);
                drawBloackR(1,15);drawBloackR(2,15);drawBloackR(6,15);drawBloackR(11,15);drawBloackR(15,15);drawBloackR(18,15);
                drawBloackR(20,15);drawBloackR(23,15);drawBloackR(26,15);drawBloackR(29,15);drawBloackR(32,15);drawBloackR(33,15);
                drawBloackR(35,15);drawBloackR(41,15);drawBloackR(46,15);drawBloackR(49,15);drawBloackR(0,16);drawBloackR(6,16);
                drawBloackR(16,16);drawBloackR(20,16);drawBloackR(24,16);drawBloackR(31,16);drawBloackR(32,16);drawBloackR(37,16);
                drawBloackR(40,16);drawBloackR(44,16);drawBloackR(46,16);drawBloackR(48,16);drawBloackR(49,16);drawBloackR(0,17);
                drawBloackR(6,17);drawBloackR(10,17);drawBloackR(14,17);drawBloackR(16,17);drawBloackR(19,17);drawBloackR(20,17);
                drawBloackR(24,17);drawBloackR(31,17);drawBloackR(34,17);drawBloackR(40,17);drawBloackR(41,17);drawBloackR(49,17);
                drawBloackR(0,18);drawBloackR(2,18);drawBloackR(4,18);drawBloackR(7,18);drawBloackR(10,18);drawBloackR(22,18);
                drawBloackR(26,18);drawBloackR(27,18);drawBloackR(31,18);drawBloackR(37,18);drawBloackR(39,18);drawBloackR(40,18);
                drawBloackR(46,18);drawBloackR(49,18);drawBloackR(0,19);drawBloackR(1,19);drawBloackR(7,19);drawBloackR(8,19);
                drawBloackR(9,19);drawBloackR(14,19);drawBloackR(16,19);drawBloackR(19,19);drawBloackR(24,19);drawBloackR(27,19);
                drawBloackR(31,19);drawBloackR(36,19);drawBloackR(39,19);drawBloackR(40,19);drawBloackR(43,19);drawBloackR(47,19);
                drawBloackR(48,19);drawBloackR(49,19);drawBloackR(0,20);drawBloackR(2,20);drawBloackR(6,20);drawBloackR(8,20);
                for (x=10; x<=13;x++)
                drawBloackR (x,20);drawBloackR(16,20);drawBloackR(19,20);drawBloackR(20,20);drawBloackR(25,20);drawBloackR(26,20);drawBloackR(28,20);
                drawBloackR(29,20);drawBloackR(30,20);drawBloackR(33,20);drawBloackR(40,20);drawBloackR(44,20);drawBloackR(49,20);
                drawBloackR(0,21);drawBloackR(1,21);drawBloackR(5,21);drawBloackR(15,21);drawBloackR(16,21);drawBloackR(19,21);
                drawBloackR(20,21);drawBloackR(27,21);drawBloackR(34,21);drawBloackR(36,21);drawBloackR(41,21);drawBloackR(46,21);
                drawBloackR(49,21);drawBloackR(0,22);drawBloackR(5,22);drawBloackR(13,22);drawBloackR(15,22);drawBloackR(26,22);
                drawBloackR(33,22);drawBloackR(36,22);drawBloackR(39,22);drawBloackR(43,22);drawBloackR(49,22);drawBloackR(0,23);
                drawBloackR(2,23);drawBloackR(5,23);drawBloackR(15,23);for (x=18; x<=22;x++)
                drawBloackR (x,23);drawBloackR(26,23);drawBloackR(29,23);drawBloackR(32,23);
                drawBloackR(36,23);drawBloackR(38,23);drawBloackR(41,23);drawBloackR(49,23);drawBloackR(0,24);drawBloackR(5,24);
                drawBloackR(9,24);drawBloackR(10,24);drawBloackR(12,24);drawBloackR(15,24);drawBloackR(18,24);drawBloackR(23,24);
                drawBloackR(31,24);drawBloackR(38,24);drawBloackR(41,24);drawBloackR(43,24);drawBloackR(44,24);drawBloackR(46,24);
                drawBloackR(49,24);drawBloackR(0,25);drawBloackR(2,25);drawBloackR(5,25);drawBloackR(15,25);drawBloackR(23,25);
                drawBloackR(30,25);drawBloackR(33,25);drawBloackR(36,25);drawBloackR(37,25);drawBloackR(38,25);drawBloackR(48,25);
                drawBloackR(49,25);drawBloackR(0,26);drawBloackR(3,26);drawBloackR(5,26);drawBloackR(12,26);drawBloackR(15,26);
                drawBloackR(19,26);drawBloackR(25,26);drawBloackR(29,26);drawBloackR(35,26);drawBloackR(39,26);drawBloackR(42,26);
                drawBloackR(49,26);drawBloackR(0,27);drawBloackR(5,27);drawBloackR(6,27);drawBloackR(17,27);drawBloackR(21,27);
                drawBloackR(22,27);drawBloackR(28,27);drawBloackR(29,27);drawBloackR(33,27);drawBloackR(41,27);drawBloackR(43,27);
                drawBloackR(49,27);drawBloackR(0,28);drawBloackR(2,28);drawBloackR(3,28);drawBloackR(6,28);drawBloackR(15,28);
                drawBloackR(20,28);drawBloackR(24,28);drawBloackR(27,28);drawBloackR(31,28);drawBloackR(33,28);drawBloackR(37,28);
                drawBloackR(42,28);drawBloackR(49,28);drawBloackR(0,29);drawBloackR(3,29);drawBloackR(7,29);drawBloackR(11,29);
                drawBloackR(14,29);drawBloackR(15,29);drawBloackR(19,29);drawBloackR(26,29);drawBloackR(34,29);drawBloackR(36,29);
                drawBloackR(37,29);drawBloackR(43,29);drawBloackR(47,29);drawBloackR(49,29);drawBloackR(0,30);drawBloackR(6,30);
                for (x=8; x<=14;x++)
                drawBloackR (x,30);drawBloackR(17,30);drawBloackR(25,30);drawBloackR(27,30);drawBloackR(28,30);drawBloackR(31,30);drawBloackR(32,30);
                drawBloackR(33,30);drawBloackR(35,30);drawBloackR(39,30);drawBloackR(45,30);drawBloackR(49,30);drawBloackR(0,31);
                for (x=11; x<=14;x++)
                drawBloackR (x,31);drawBloackR(24,31);drawBloackR(26,31);drawBloackR(29,31);drawBloackR(32,31);drawBloackR(34,31);drawBloackR(49,31);
                drawBloackR(0,32);drawBloackR(2,32);drawBloackR(7,32);drawBloackR(8,32);for (x=15; x<=23;x++)
                drawBloackR (x,32);drawBloackR(25,32);drawBloackR(31,32);
                drawBloackR(32,32);drawBloackR(36,32);drawBloackR(38,32);drawBloackR(39,32);drawBloackR(40,32);drawBloackR(42,32);
                drawBloackR(45,32);drawBloackR(46,32);drawBloackR(49,32);drawBloackR(0,33);drawBloackR(5,33);drawBloackR(6,33);
                drawBloackR(16,33);drawBloackR(31,33);drawBloackR(33,33);drawBloackR(49,33);drawBloackR(0,34);drawBloackR(2,34);
                drawBloackR(5,34);drawBloackR(10,34);drawBloackR(15,34);drawBloackR(21,34);drawBloackR(24,34);drawBloackR(25,34);
                drawBloackR(27,34);drawBloackR(29,34);drawBloackR(34,34);drawBloackR(37,34);drawBloackR(0,35);drawBloackR(10,35);
                drawBloackR(14,35);drawBloackR(17,35);drawBloackR(26,35);drawBloackR(30,35);drawBloackR(33,35);drawBloackR(36,35);
                drawBloackR(41,35);drawBloackR(44,35);drawBloackR(49,35);drawBloackR(0,36);drawBloackR(1,36);drawBloackR(3,36);
                drawBloackR(6,36);drawBloackR(7,36);drawBloackR(12,36);drawBloackR(19,36);drawBloackR(23,36);drawBloackR(24,36);
                drawBloackR(26,36);drawBloackR(27,36);drawBloackR(38,36);drawBloackR(42,36);drawBloackR(45,36);drawBloackR(46,36);
                drawBloackR(48,36);drawBloackR(0,37);drawBloackR(11,37);drawBloackR(14,37);drawBloackR(19,37);drawBloackR(21,37);
                drawBloackR(22,37);drawBloackR(39,37);drawBloackR(42,37);drawBloackR(47,37);drawBloackR(49,37);drawBloackR(0,38);
                drawBloackR(3,38);drawBloackR(6,38);drawBloackR(16,38);drawBloackR(18,38);drawBloackR(42,38);for (x=0; x<=42;x++)
                drawBloackR (x,39);*/
            break;

            case 2:
            drawBloackR(0, 0);drawBloackR(1, 0);drawBloackR(20,0);drawBloackR(21,0);drawBloackR(0, 1);
            drawBloackR(21,1);
            for (x=6;x<=15;x++)drawBloackR(x,4);
            for (x=6;x<=15;x++)drawBloackR(x,7);drawBloackR(0, 10);drawBloackR(21,10);drawBloackR(0, 11);drawBloackR(1, 11);drawBloackR(20,11);
            drawBloackR(21,11);
            break;

            case 3:
            drawBloackR(7, 0);drawBloackR(7, 1);drawBloackR(7, 2);
            for (x=14; x<=21;x++)drawBloackR(x,2);drawBloackR(7, 3);drawBloackR(7, 4);
            drawBloackR(7, 5);drawBloackR(14, 6);drawBloackR(14, 7);drawBloackR(14, 8);
            for (x=0;x<=7;x++)drawBloackR(x,9);drawBloackR(14, 9);drawBloackR(14, 10);
            drawBloackR(14, 11);
            break;
            case 4:
            for (x=0; x<=21;x++)drawBloackR(x,0);drawBloackR(0, 1);drawBloackR(21,1);drawBloackR(0, 2);drawBloackR(6, 2);drawBloackR(15, 2);
            drawBloackR(21,2);drawBloackR(0, 3);drawBloackR(6, 3);
            for (x=8;x<=13;x++)drawBloackR(x,3);drawBloackR(15, 3);drawBloackR(21,3);drawBloackR(0, 8);
            drawBloackR(6, 8);
            for (x=8;x<=13;x++)drawBloackR(x,8);drawBloackR(15, 8);drawBloackR(21,8);drawBloackR(0, 9);drawBloackR(6, 9);drawBloackR(15, 9);
            drawBloackR(21,9);drawBloackR(0, 10);drawBloackR(21,10);
            for (x=0; x<=21;x++)drawBloackR(x,11);
            break;
            case 5:
            drawBloackR(0, 0);drawBloackR(1, 0);drawBloackR(2, 0);
            for (x=5;x<=18;x++)drawBloackR(x,0);drawBloackR(0, 1);drawBloackR(9, 1);
            drawBloackR(9, 2);drawBloackR(9, 3);
            for (x=0;x<=9;x++)drawBloackR(x,4);
            for (x=12; x<=21;x++)drawBloackR(x,4);
            for (x=0; x<=21;x++)drawBloackR(x,7);drawBloackR(11, 8);drawBloackR(11, 9);drawBloackR(11, 10);drawBloackR(11, 11);
            break;
            case 6:
            drawBloackR(11, 0);drawBloackR(11, 1);drawBloackR(10, 2);drawBloackR(11, 2);drawBloackR(12, 2);
            drawBloackR(11, 3);drawBloackR(11, 4);drawBloackR(7, 5);drawBloackR(11, 5);drawBloackR(15, 5);
            for (x=0; x<=21;x++)drawBloackR(x,6);drawBloackR(11, 7);
            drawBloackR(11, 8);drawBloackR(10, 9);drawBloackR(11, 9);drawBloackR(12, 9);drawBloackR(11, 10);drawBloackR(11, 11);
            break;
            case 7:
            drawBloackR(12, 0);drawBloackR(13, 0);drawBloackR(14, 0);drawBloackR(12, 1);drawBloackR(3, 2);
            drawBloackR(7, 2);drawBloackR(12, 2);
            for (x=0;x<=9;x++)drawBloackR(x,3);
            for (x=12;x<=18;x++)drawBloackR(x,3);drawBloackR(12, 4);drawBloackR(7, 5);drawBloackR(7, 6);
            for (x=2;x<=7;x++)drawBloackR(x,7);
            for (x=10; x<=21;x++)drawBloackR(x,7);drawBloackR(7, 8);
            drawBloackR(12, 8);drawBloackR(17, 8);drawBloackR(7, 9);drawBloackR(12, 9);drawBloackR(5, 10);drawBloackR(6, 10);
            drawBloackR(7, 10);drawBloackR(7, 11);
            break;
            case 8:
            drawBloackR(6, 0);drawBloackR(15, 0);drawBloackR(5, 1);drawBloackR(16, 1);drawBloackR(4, 2);
            drawBloackR(17, 2);drawBloackR(3, 3);drawBloackR(18, 3);drawBloackR(2, 4);drawBloackR(19, 4);drawBloackR(1, 5);
            drawBloackR(20, 5);drawBloackR(0, 6);drawBloackR(21,6);
            for (x=0;x<=6;x++)drawBloackR(x,7);
            for (x=15; x<=21;x++)drawBloackR(x,7);drawBloackR(6, 8);drawBloackR(15, 8);
            break;
            case 9:
            drawBloackR(3, 1);drawBloackR(5, 1);drawBloackR(6, 1);drawBloackR(14, 1);drawBloackR(15, 1);
            drawBloackR(17, 1);drawBloackR(3, 2);drawBloackR(5, 2);drawBloackR(6, 2);drawBloackR(14, 2);drawBloackR(15, 2);
            drawBloackR(3, 3);
            for (x=14;x<=17;x++)drawBloackR(x,3);
            for (x=3;x<=6;x++)drawBloackR(x,4);
            for (x=14;x<=17;x++)drawBloackR(x,4);
            for (x=0;x<=4;x++)drawBloackR(x,6);drawBloackR(21,6);drawBloackR(7, 7);drawBloackR(9, 7);drawBloackR(11, 7);
            for (x=13; x<=21;x++)drawBloackR(x,7);drawBloackR(13, 8);
            drawBloackR(21,9);
            for (x=1;x<=19;x++)drawBloackR(x,10);drawBloackR(21,10);drawBloackR(21,11);
            break;
            case 10:
            for (x=0;x<=5;x++)drawBloackR(x,2);
            for (x=13; x<=21;x++)drawBloackR(x,2);drawBloackR(9, 4);drawBloackR(10, 4);
            for (x=8;x<=11;x++)drawBloackR(x,5);
            for (x=7;x<=12;x++)drawBloackR(x,6);drawBloackR(0, 7);drawBloackR(1, 7);drawBloackR(3, 7);
            drawBloackR(4, 7);drawBloackR(7, 7);drawBloackR(12, 7);drawBloackR(14, 7);drawBloackR(15, 7);drawBloackR(17, 7);
            drawBloackR(18, 7);drawBloackR(20,7);drawBloackR(21,7);drawBloackR(7, 8);drawBloackR(12, 8);drawBloackR(7, 9);
            drawBloackR(12, 9);
            for (x=1;x<=7;x++)drawBloackR(x,10);
            for (x=12;x<=20;x++)drawBloackR(x,10);
            break;
            case 11:
            for (x=0; x<=21;x++)drawBloackR(x,0);drawBloackR(0, 1);drawBloackR(21,1);drawBloackR(0, 2);drawBloackR(4, 2);drawBloackR(7, 2);
            drawBloackR(10, 2);drawBloackR(13, 2);drawBloackR(16, 2);drawBloackR(19, 2);drawBloackR(21,2);drawBloackR(0, 3);
            drawBloackR(21,3);drawBloackR(0, 4);drawBloackR(21,4);drawBloackR(0, 5);drawBloackR(21,5);drawBloackR(0, 6);
            drawBloackR(21,6);drawBloackR(0, 7);drawBloackR(21,7);drawBloackR(0, 8);drawBloackR(21,8);drawBloackR(0, 9);
            drawBloackR(2, 9);drawBloackR(5, 9);drawBloackR(8, 9);drawBloackR(11, 9);drawBloackR(14, 9);drawBloackR(17, 9);
            drawBloackR(21,9);drawBloackR(0, 10);drawBloackR(21,10);
            for (x=0; x<=21;x++)drawBloackR(x,11);
            break;
        }
        repaint();
        dibujardirecto=true;
    }

    public int intervalSnake(int nivel){
        int valor=0;
        final int velnivel=643;
        final byte agrnivel=11;
        byte x;
        int vp=velnivel,ap=agrnivel;

        for (x = 1; x <= nivel; x++) {
            ap-=10;
            vp-=Math.abs(ap);
        }
        valor=vp;
        if (valor<0){
            valor=25-((nivel-14)*2);
        }
        if (valor<=0)valor=1;
        //System.out.println("Intervalo"+valor);
        return valor;
    }
    public void dibujarRelleno(NodoCuerpo nc){
        prox=new javax.swing.ImageIcon(getClass().getResource("/extras/r.png")).getImage();
        dibujar(nc);
    }


///////////////////////////////////
    public void drawBlock(int x, int y){
        prox=new javax.swing.ImageIcon(getClass().getResource("/extras/b"+ this.idBloque+".png")).getImage();
        x=(int)(x/tcuadrox);
        y=(int)(y/tcuadroy);
        dibujar(x, y);
        //System.out.println("Dibujado en " +x + ":" + y);
        dibujarBordeBloque(x, y,true);

    }
    private void drawBloackR(int x, int y){
        prox=new javax.swing.ImageIcon(getClass().getResource("/extras/b"+ this.idBloque+".png")).getImage();
        dibujar(x, y);
        dibujarBordeBloque(x, y,true);
    }
    private void dibujarBloque(int x,int y, boolean consecutivos){
        prox=new javax.swing.ImageIcon(getClass().getResource("/extras/b"+ this.idBloque+".png")).getImage();
        dibujar(x, y);
        dibujarBordeBloque(x, y, consecutivos);
    }

    private void dibujarBordeBloque(int x, int y, boolean consecutivos){
        //System.out.println("Va a dibujar bordes");
            NodoCuerpo nc=new NodoCuerpo();
        for (int i=1;i<=4;i++){
            nc.setXY(x, y);nc.dir=i;
            siguientepos(nc);
            if (!getCuadrocupado(nc)){
                prox=new javax.swing.ImageIcon(getClass().getResource("/extras/br"+ i+".png")).getImage();
                borrarAntesDeDibujar=false;
               // System.out.println("No ocupado "+ nc.toString());
                dibujar(x,y);
            }else if (consecutivos){
                //System.out.println("Dibujar consecutivo "+ consecutivos + " " + nc.x +" +"+nc.y);
                dibujarBloque(nc.x, nc.y,false);

            }
        }

//////////////////////////////////

    }


    ////////////////////////////////
    ////////////////////////////////
    public void dibujarDestino(int x, int y){
        if (destino!=null && destino.x!=-1)borrar(destino);
        prox=new javax.swing.ImageIcon(getClass().getResource("/extras/destino.png")).getImage();
        x=(int)(x/tcuadrox);
        y=(int)(y/tcuadroy);
        dibujar(x, y);
        destino.setXY(x, y);
        this.cuadrocupado[x][y]=false;
        espaciosNoOcupados++;
    }
    public void dibujarDestinoR(int x,int y){
        if (destino!=null && destino.x!=-1)borrar(destino);
        prox=new javax.swing.ImageIcon(getClass().getResource("/extras/destino.png")).getImage();
        dibujar(x, y);
        destino.setXY(x, y);
        this.cuadrocupado[x][y]=false;
        espaciosNoOcupados++;
    }
    public void dibujarOrigen(int x, int y){
        if (origen!=null && origen.x!=-1)borrar(origen);
        prox=new javax.swing.ImageIcon(getClass().getResource("/extras/origen.png")).getImage();
        x=(int)(x/tcuadrox);
        y=(int)(y/tcuadroy);
        dibujar(x, y);
        origen.setXY(x, y);
        this.cuadrocupado[x][y]=false;
        espaciosNoOcupados++;
    }
    public void dibujarOrigenR(int x, int y){
        if (origen!=null && origen.x!=-1)borrar(origen);
        prox=new javax.swing.ImageIcon(getClass().getResource("/extras/origen.png")).getImage();
        dibujar(x, y);
        origen.setXY(x, y);
        this.cuadrocupado[x][y]=false;
        espaciosNoOcupados++;
    }
    public void dibujarVit(){
        prox=new javax.swing.ImageIcon(getClass().getResource("/extras/r"+ world.moviVit+ ".png")).getImage();
        dibujar(world.Vit);
    }

    public void dibujarAmplitud(NodoCuerpo nc){
        prox=new javax.swing.ImageIcon(getClass().getResource("/extras/r2.png")).getImage();
        dibujar(nc);
        this.cuadrocupado[nc.x][nc.y]=false;
        espaciosNoOcupados++;
    }
    //////////////////////
    /////////////////////
    public NodoCuerpo crearBloque(int x, int y){
        return new NodoCuerpo((int)(x/tcuadrox), (int)(y/tcuadroy), 0);
    }
    public Game getWorld() {
        return world;
    }    public void setWorld(Game world) {
        this.world = world;
    }

    public String codigoLaberinto(){
        ListaNodos puntos=new ListaNodos();
        int x=0,y=0,z=0,otro = 0;
        int num=0;
        StringBuilder sb=new StringBuilder();
        sb.append("//Inicio\r\n");
        for (y = 0; y < this.getScaleHeight(); y++) {
            for (x = 0; x < this.getScaleWidth(); x++) {
                if (this.getCuadrocupado(x, y)){
                    num++;
                    puntos.insertar(new NodoCuerpo(x, y, 0));
                    if (x==this.getScaleWidth()-1){
                        if (num>=4){
                            sb.append("for (x="+puntos.getValor(1).x+";"+" x<="+ puntos.getValor(puntos.getTamano()).x +";x++)");
                            sb.append("drawBloackR (canvas, x,"+puntos.getValor(1).y+");");
                        }else{
                            while(z<num){
                                z++;otro++;
                                if (otro==6){
                                    sb.append("drawBloackR(canvas, "+puntos.getValor(z).x+","+puntos.getValor(1).y+");").append("\r\n");
                                    otro=0;
                                }else{
                                    sb.append("drawBloackR(canvas,"+puntos.getValor(z).x+","+puntos.getValor(1).y+");");
                                }
                            }
                            z=0;
                        }
                        num=0;
                        puntos.vaciar();
                    }
                }else{
                    if (num>=4){
                        sb.append("for (x="+puntos.getValor(1).x+";"+" x<="+ puntos.getValor(puntos.getTamano()).x +";x++)");
                        sb.append("drawBloackR (canvas, x,"+puntos.getValor(1).y+");");
                    }else{
                        while(z<num){
                            z++;otro++;
                            if (otro==6){
                                sb.append("drawBloackR(canvas, "+puntos.getValor(z).x+","+puntos.getValor(1).y+");").append("\r\n");
                                otro=0;
                            }else{
                                sb.append("drawBloackR(canvas, "+puntos.getValor(z).x+","+puntos.getValor(1).y+");");
                            }
                        }
                        z=0;
                    }
                    num=0;
                    puntos.vaciar();
                }
            }
        }
        sb.append("\r\n//teminado");
        System.out.println(sb.toString());
        return sb.toString();
    }
    public boolean[][] getCuadrocupado() {
        return cuadrocupado;
    }
    public boolean [][]getMatriz(){
        boolean matriz[][]=new boolean[this.cuadrocupado.length][this.cuadrocupado[0].length];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                matriz[i][j] = this.cuadrocupado[i][j];
            }
        }
        return matriz;
    }

     public void siguientepos(NodoCuerpo nc) {
        switch (nc.dir) {
            case 1:
                nc.y++;
                if (nc.y >= getScaleHeight()) {
                    nc.y = 0;
                }
                break;
            case 2:
                nc.x++;
                if (nc.x >= getScaleWidth()) {
                    nc.x = 0;
                }
                break;
            case 3:
                nc.y--;
                if (nc.y < 0) {
                    nc.y = getScaleHeight() - 1;
                }
                break;
            case 4:
                nc.x--;
                if (nc.x < 0) {
                    nc.x = getScaleWidth() - 1;
                }
                break;
        }
    }
    public boolean cuadrosJuntos(NodoCuerpo n1,NodoCuerpo n2){
        for (int i = 1; i <=4; i++) {
            NodoCuerpo nc=n1.clone();
            nc.dir=i;
            siguientepos(nc);
            if (nc.igualPosicion(n2))return true;
        }
        return false;
    }
    public BodySnake getBody() {
        return body;
    }
}
////////////////////////////////
class ListaNodos{
    NodoCuerpo ptr;
    NodoCuerpo fin;
    private int tamano=0;
    public void insertar(NodoCuerpo nodo){
        if (ptr==null){
            ptr=nodo;
            fin=nodo;
        }else{
            fin.setSig(nodo);
            fin=fin.getSig();
        }
        tamano++;
    }
    public NodoCuerpo getValor(int pos){
        NodoCuerpo ret=null;
        if (ptr!=null){
            if (ptr.getSig()==null){
                return ptr.clone();
            }else{
                NodoCuerpo tmptr=ptr;
                int rec=0;
                do{
                    rec++;
                    if (rec==pos){
                        return tmptr.clone();
                    }
                    tmptr=tmptr.getSig();
                }while(tmptr!=null);
            }
        }
        return ret;
    }

    public int getTamano() {
        return tamano;
    }
    public void vaciar(){
        ptr=null;
        fin=null;
        this.tamano=0;
    }

}