package extras;

public class NodoLista {
    private NodoLista sig;//lista doblemente enlazada
    private NodoLista ant;
    private Object obj;
    public NodoLista(Object obj) {
        this.obj = obj;
    }    public NodoLista getSig() {
        return sig;
    }    public void setSig(NodoLista sig) {
        this.sig = sig;
    }    public NodoLista getAnt() {
        return ant;
    }    public void setAnt(NodoLista ant) {
        this.ant = ant;
    }    public Object getObj() {
        return obj;
    }    public void setObj(Object obj) {
        this.obj = obj;
    }
}
