package extras;

/**
 * @author BS
 */
public class ListaObjeto {
//Esta clase es para manejar conjunto de controles
    //inicialmente la cree para generar el codigo de los laberintos, despues de haberlo dibujado

    NodoLista ptr;
    NodoLista fin;
    private int tamano = 0;

    public ListaObjeto() {
    }

    public void insertar(Object obj) {
        NodoLista nl = new NodoLista(obj);
        if (ptr == null) {
            ptr = nl;
            fin = nl;
        } else {
            fin.setSig(nl);
            fin = fin.getSig();
        }
        tamano++;
    }

    public Object getPtr() {
        return ptr;
    }

    public Object getFin() {
        return fin;
    }

    public Object getObject(int pos) {
        NodoLista ret = null;
        if (ptr != null) {
            if (ptr.getSig() == null) {
                return ptr.getObj();
            } else {
                NodoLista tmptr = ptr;
                int rec = 0;
                do {
                    rec++;
                    if (rec == pos) {
                        return tmptr.getObj();
                    }
                    tmptr = tmptr.getSig();
                } while (tmptr != null);
            }
        }
        return ret;
    }

    public Object getObjectCasting(int pos) {
        NodoLista nl = (NodoLista) getObject(pos);
        return nl.getObj();
    }

    public int getTamano() {
        return tamano;
    }

    public void vaciar() {
        ptr = null;
        fin = null;
        this.tamano = 0;
    }

}
