/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package codigo;

/**
 *
 * @author BS
 */
public class NodoIaTree extends  NodoCuerpo{
    //Este vector es el que desencadena el conjunto de Nodos Arboles...todos relacionados entre la raiz
    private NodoIaTree posdir[]=new NodoIaTree[4];

    private int numCruces=0;
    //Es el numero de recorridos o espacios que ha hecho el Nodo Arbol para llegar al lugar en donde está
    int nr=0;
    //Es el Nodo Raiz que contiene esta rama

    private int dirInicial=0;//Es la dirección que cogió la raíz principal para llegar a este lugar
    private NodoIaTree root=null;

    //Usados para crear una lista de Nodos Arbol...donde cada Nodo no tiene relación alguna con el siguiente
    ////////////////////////////
    private NodoIaTree sig=null;
    private NodoIaTree ant=null;
    ////////////////////////////

    public NodoIaTree(int x, int y, int dir) {
        super(x, y, dir);
    }
    public NodoIaTree(NodoCuerpo nc){
        super(nc.x,nc.y,nc.dir);
    }
    public NodoIaTree(NodoIaTree nc){
        super(nc.x,nc.y,nc.dir);
        this.numCruces=nc.numCruces;
    }

    public void vaciar(){
        posdir=new NodoIaTree[4];
    }
    public boolean estaenrecorrido(NodoCuerpo nc){
        NodoIaTree troot=getRoot();
        while (troot!=null ){
            if (troot.igualPosicion(nc)){
                return true;
            }
            troot=troot.getRoot();
        }
        return false;
    }
    public NodoIaTree getRoot() {
        return root;
    }
    public boolean sinDireccion(){
        int nn=0;
        for (int i = 0; i <=3; i++) {
            if (i+1!=dir && this.posdir[i]==null){
                nn++;
            }
        }
        if (nn==3)return true;
        else return false;
    }
    public void setRoot(NodoIaTree root) {
        this.root = root;
    //    this.nr=root.nr+1;
        this.dirInicial=root.dirInicial;
    }

    @Override
    public NodoIaTree getSig() {
        return this.sig;
    }    public void setSig(NodoIaTree sig) {
        this.sig = sig;
    }    @Override
    public NodoIaTree getAnt() {
        return ant;
    }    public void setAnt(NodoIaTree ant) {
        this.ant = ant;
    }

    public NodoIaTree getPosdir(int dir ) {
        return posdir[dir-1];
    }
    public void setPosdir(int dir, NodoIaTree posdir) {
        posdir.nr=this.nr+1;
        if (posdir.dir!=this.dir)posdir.numCruces++;
        posdir.dirInicial=this.dirInicial;
        this.posdir[dir-1] = posdir;
        posdir.setRoot(this);
    }
    public int getDirInicial() {
        return dirInicial;
    }public void setDirInicial(int dirInicial) {
        this.dirInicial = dirInicial;
    }
    public int getNumCruces() {
        return numCruces;
    }
    public void setNumCruces(int numCruces) {
        this.numCruces = numCruces;
    }
}
