package codigo;

/**
 * @author BS.Corp
 */
public class BodySnake {

    private NodoCuerpo cabeza;
    private NodoCuerpo cola;
    private NodoCuerpo ultcola;//la ultima cola eliminada es guardada
    private Game world;
    private Laberinto lab;
    private int tamano = 0;

    public void Cabeza(NodoCuerpo nc) {
        cabeza = nc;
        cola = nc;
        tamano = 1;
    }

    public void eliminarCola() {
        if (cola.getAnt() != null) {
            ultcola = cola;//guardo la referencia de la cola antes de eliminarla
            cola = cola.getAnt();
            cola.setSig(null);
        }
    }

    public void mover_y_o_crecer() {
        if (cabeza != null) {
            NodoCuerpo nc = new NodoCuerpo(cabeza.x, cabeza.y, cabeza.dir);//coge una copia de la cabeza
            cabeza.setAnt(nc);//une la cabeza con el nuevo nodo
            nc.setSig(cabeza);//y el nuevo nodo con la cabeza
            cabeza = nc;//le coloca la cabeza al nuevo nodo
            siguientepos();
            if (world.comioVit == false) {//si no ha comido Vitamina elimina la cola
                eliminarCola();
                lab.quitarColaEliminada();
                lab.dibujarCola();
            } else {
                tamano++;
                world.comioVit = false;
            }
        }

    }

    public NodoCuerpo getCabeza() {//Esta funcion retorna el nodo de la cabeza, es necesario cuando la lista no va a recorrerse
        ///a si misma, sino que otra clase va a hacer el correspondiente recorrido
        return cabeza;
    }

    public NodoCuerpo getCola() {
        return cola;
    }

    public NodoCuerpo ultCola() {
        return ultcola;
    }

    public void setLab(Laberinto lab) {
        this.lab = lab;
    }

    public void siguientepos() {
        this.lab.siguientepos(cabeza);
    }
    public boolean enCuadroOcupado() {
        if (lab.getCuadrocupado(cabeza.x, cabeza.y) == true) {
            return true;
        } else {
            return false;
        }
    }

    public int getTamano() {
        return tamano;
    }

    /**
     * @return the world
     */
    public Game getWorld() {
        return world;
    }

    /**
     * @param world the world to set
     */
    public void setWorld(Game world) {
        this.world = world;
    }

    public NodoCuerpo getXY(int posInv) {
        NodoCuerpo vreturn = null;
        int con = 1;
        if (posInv == 1) {
            return cola.clone();
        } else {
            NodoCuerpo tcola = cola;
            while (cola.getAnt() != null) {
                cola = cola.getAnt();
                con++;
                if (con == posInv) {
                    return tcola.clone();
                }
            }
        }
        return vreturn;
    }
    public boolean estaEnCulebra(int posInvMax, NodoCuerpo nc){
        boolean v=false;
        NodoCuerpo tmpcola=this.cola;
        int con=0;
        do{
            con++;
            if (tmpcola.igualPosicion(nc)){
                return true;
            }
            tmpcola=tmpcola.getAnt();
        }while(con<posInvMax && tmpcola!=null);
        return v;
    }
    public boolean estaEnCulebra(NodoCuerpo nc){
        return estaEnCulebra(tamano, nc);
    }
}
