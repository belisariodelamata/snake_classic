

package codigo;
/**
 * @author Bs-Corp
 */
public class NodoCuerpo {
    public int x;
    public int y;
    public int dir;
    /*
        Abajo = 4
        Derecha=1
        Arriba
        Izquierda
    */
    private NodoCuerpo sig;//lista doblemente enlazada
    private NodoCuerpo ant;

    public NodoCuerpo() {

    }
    public void setXY(int x, int y){
        this.x=x;this.y=y;
    }
    @Override
    public NodoCuerpo clone(){
        NodoCuerpo nc=new NodoCuerpo(x, y, dir);
        return nc;
    }

    public NodoCuerpo(int x, int y, int dir) {
        this.x = x;
        this.y = y;
        this.dir = dir;
    }
    public NodoCuerpo getSig() {
        return sig;
    }
    public void setSig(NodoCuerpo sig) {
        this.sig = sig;
    }
    public NodoCuerpo getAnt() {
        return ant;
    }
    public void setAnt(NodoCuerpo ant) {
        this.ant = ant;
    }
    public boolean igualPosicion(NodoCuerpo nc){
        if (x==nc.x && y==nc.y)return true;
        else return false;
    }

    @Override
    public String toString() {
        return this.x+":"+this.y+"-"+this.dir;
    }
    public void chDirRel(int idir) {//cambiar direccion del nodo, empezando que idir=1 es la derecha teniendo en cuenta la direccion;
        //si el nodo tuviera direccion de izquierda, se cambiara la direccion a la derecha, cogeria hacia arriba
        //si fuese hacia abajo, cogeria hacia la izquierda, y asi sucesivamente
        dir += idir;
        if (dir > 4) {
            dir -= 4;
        }
    }
    public void chDirAbsLegal(int idir){
        //cambia la direccion absoluta, si recibe 1 dobla hacia la derecha, esté apuntando a donde sea la direccion
        if (getDirRel(2)!=idir){//si la direccion a donde quiere apuntar no es la contraria de la que tiene
            dir=idir;
        }
    }
    public int getDirRel(int idir){
        int tdir=dir;
        tdir+=idir;
        if (tdir>4){
            tdir-=4;
        }
        return tdir;
    }
}