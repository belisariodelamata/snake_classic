/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

/**
 *
 * @author BS
 */
//Esta clase lo que realmente hace es calcular la cantidad de espacios en blanco que\
//existen a partir de un bloque...
//para hacerlo verifica las cuadro direcciones del bloque original y empieza a ir sumando
//a la final existiran un vector que tendra guardado la cantidad de espacios en una direccion...
//Normalmente elijo de direccion de culebra la que tenga mayor espacios para moverse..aunque a veces no es asi...
//pero hay casos en los que en un camino cerrado es mejor elegir otra direccion....
public class ListaInteligencia {

    private Laberinto lab;
    private Game world1;
    private boolean cuadrocupado[][] = null;
    private NodoCuerpo ptr = null;
    private NodoCuerpo fin = null;
    private int blancos[] = new int[4];
    private int dir = 1;
    ////Vector de las posibles direcciones a tomar
    public int dirInteligencia[] = null;
    public int numdirInteligencia = 0;//cantidad de direcciones guardadas en el anterior vector
    ////
    public int dirInteligencia2[] = null;
    public int numdirInteligencia2 = 0;
    ////
   // private NodoCuerpo ext[]=new NodoCuerpo[4];
    ////


    public ListaInteligencia(NodoCuerpo nc, Game world1) {
        listaInteligencia(nc, world1);
    }

    private void listaInteligencia(NodoCuerpo nc, Game world1) {
        this.lab = world1.getLab();
        this.world1=world1;
        for (dir = 1; dir <= 4; dir++) {
            cuadrocupado = new boolean[this.lab.getScaleWidth()][this.lab.getScaleHeight()];
            vaciar();
            this.cuadrocupado[nc.x][nc.y] = true;
            NodoCuerpo tnc = nc.clone();//sacamos un clone al nodo de referencia
            tnc.dir = dir;//le cambiamos la direccion de verificacion
            siguientepos(tnc);//le colocamos la posicion correspondiente a la direccion
            if (!this.cuadrOcupado(tnc)) {
                insertarInicio(tnc);
                this.cuadrocupado[tnc.x][tnc.y] = true;
                while (ptr != null) {
                    NodoCuerpo tmptr = ptr.clone();
                    eliminarPtr();
                    insertarBloquesAlrededor(tmptr);
                }
            }
        }
    }

    public ListaInteligencia(NodoCuerpo nc, Game world1, NodoCuerpo nb) {
        listaInteligencia(nc, world1);
    }

    public boolean cuadrOcupado(NodoCuerpo nc) {
        if ((this.cuadrocupado[nc.x][nc.y]==false) && (nc.igualPosicion(world1.Vit) || nc.igualPosicion(world1.Bono))){
            return false;
        }
        else return this.cuadrocupado[nc.x][nc.y] || this.lab.getCuadrocupado(nc);
    }

    public void insertarBloquesAlrededor(NodoCuerpo nc) {
        for (int x = 1; x <= 4; x++) {
            NodoCuerpo tnc = nc.clone();
            tnc.dir = x;
            siguientepos(tnc);
            if (this.cuadrOcupado(tnc) == false) {
                this.cuadrocupado[tnc.x][tnc.y] = true;
                insertarInicio(tnc);
            }
        }
    }

    public int blancosenDir(int dir) {
        return this.blancos[dir - 1];
    }

    public int dirInteligencia() {
        dirInteligencia = new int[4];//4 direcciones de inteligencia
        numdirInteligencia = 1;
        int dirInteligente = 1;//direccion inteligencia
        dirInteligencia[numdirInteligencia - 1] = dirInteligente;
        for (int x = 1; x <= 3; x++) {
            if (this.blancos[x] > this.blancos[dirInteligente - 1]) {
                dirInteligente = x + 1;
                dirInteligencia = new int[4];
                numdirInteligencia = 1;
                dirInteligencia[numdirInteligencia - 1] = dirInteligente;
            } else if (this.blancos[x] == this.blancos[dirInteligente - 1]) {
                dirInteligente = x + 1;
                numdirInteligencia++;
                dirInteligencia[numdirInteligencia - 1] = x + 1;
            }
        }
        return dirInteligente;
    }

    public boolean estaDirenInteligencia(int dir) {
        boolean v = false;
        for (int i = 0; i < this.numdirInteligencia; i++) {
            if (this.dirInteligencia[i] == dir) {
                v = true;
                break;
            }
        }
        return v;
    }

    private void eliminarPtr() {
        if (ptr != null) {
            ptr = ptr.getSig();
            if (ptr == null) {
                fin = null;
            }
        }
    }

    public void insertarFin(NodoCuerpo nc) {
        if (ptr == null) {
            ptr = nc;
            fin = nc;
        } else {
            fin.setSig(nc);
            fin = fin.getSig();
        }
        this.lab.dibujarRelleno(nc);
    }

    public void insertarInicio(NodoCuerpo nc) {
        if (ptr == null) {
            ptr = nc;
            fin = nc;
        } else {
            nc.setSig(ptr);
            ptr = nc;
        }
        this.blancos[dir - 1]++;
    }

    public void siguientepos(NodoCuerpo nc) {
        switch (nc.dir) {
            case 1:
                nc.y++;
                if (nc.y >= lab.getScaleHeight()) {
                    nc.y = 0;
                }
                break;
            case 2:
                nc.x++;
                if (nc.x >= lab.getScaleWidth()) {
                    nc.x = 0;
                }
                break;
            case 3:
                nc.y--;
                if (nc.y < 0) {
                    nc.y = lab.getScaleHeight() - 1;
                }
                break;
            case 4:
                nc.x--;
                if (nc.x < 0) {
                    nc.x = lab.getScaleWidth() - 1;
                }
                break;
        }
    }

    public void vaciar() {
        ptr = null;
        fin = null;
    }
}
