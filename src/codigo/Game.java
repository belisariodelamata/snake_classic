/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

public class Game {

    private Laberinto lab = null;
    //utilizo el mismo tipo de dato del cuerpo, porque este contiene
    //coordenadas, pero no utilizaré para nada la direccion
    //ni la estructura del nodo siguiente
    /////////////////
    ////////////////////////////
    NodoCuerpo Bono = new NodoCuerpo(-1, -1, -1);
    NodoCuerpo Vit = new NodoCuerpo(-1, -1, -1);
    ////////////////////////////////////
    byte movCabeza = 0;//imagen de la cabeza para el movimiento supuesto de la boca abierta
    byte movCuerpo = 0;//imagen de la cabeza para el movimiento supuesto de la boca abierta
    byte movCola = 0;//imagen de la cabeza para el movimiento supuesto de la boca abierta
    byte moviVit = 0;//Variable para el cambio de imagen de la vitamina

    public byte modo = 1;//modo de juego, campaña o clasico
    public byte nivelActual = 11;//
    public byte nivelesClasico = 11;
    public byte nivelesCampana = 11;
    private byte progresoCampana;
    public int laberintoActualClasico = 0;
    public int laberintoActualCampana = 0;
    private byte numVit;
    private byte numVitCam;

    private boolean esperando = false;//valida cuando se ejecuta el temporizador para no estrellarse
    private boolean puedePausar = false;//puede pausar el juego
    private boolean estaDetenido = false;//juego detenido
    private boolean proximoPausar = false;//guarda si en el siguiente movimiento se tendra
    //que pausar
    private int proximoMov;
    boolean estaJugando = false;//esta jugando
    private boolean puedeMoverla = false;//puede mover la culebra

    public boolean buscarAutomatico = false;

    private int puntaje;
    private byte movBono;
    boolean salioBono;
    boolean comioVit = false;//comio vitamina
    boolean mostrarLlenura = false;//mostrar la barriga
    boolean mostrarDireccion = false;//mostrar direccion

    private BodySnake snake;
    BodySnake snake2;

    public NodoCuerpo origen = new NodoCuerpo();
    public NodoCuerpo destino = new NodoCuerpo();

    private javax.swing.JLabel lblep = null;
    private javax.swing.JLabel lblbono = null;
    private javax.swing.JLabel lblpuntaje = null;
    private javax.swing.JLabel lblprogresovit = null;
    private javax.swing.JPanel panelJuego = null;
    private javax.swing.JFrame frame = null;
    codigo.IAtree ia = null;

    NodoCuerpo vitAnt = new NodoCuerpo();

    public Game() {
        temporizador.setRepeats(false);
    }

    public void iniciar(int forma) {
        snake = new BodySnake();//creo la culebra
        getSnake().setWorld(this);
        this.setLab(new Laberinto(panelJuego, 22, 12, getSnake()));//creo un laberinto y lo relaciono con la culebra
        this.getLab().setWorld(this);
        this.getLab().setMovSnake(movsnake);
        this.getLab().setCambioVit(movVit);
        this.lblbono.setVisible(false);

        getSnake().setLab(this.getLab());//la culebra la relaciono con el laberinto

        //forma 1--indica nuevo juego
        //forma 2--indica que se está jugando campaña y se debe cambiar de laberinto
        if (forma == 1) {//Si se va a iniciar un nuevo juego
            this.numVit = 0;
            this.numVitCam = 0;
            this.puntaje = 0;
            if (this.modo == 1) {//si se va iniciar un nuevo clásico
                lblprogresovit.setVisible(false);
                movsnake.setDelay(this.getLab().intervalSnake(this.nivelesClasico));//esto debo cambiarlo
            } else {
                lblprogresovit.setVisible(true);
                movsnake.setDelay(this.getLab().intervalSnake(this.nivelesCampana));//esto debo cambiarlo
                this.progresoCampana = 10;
                this.laberintoActualCampana = -1;
            }
        }
        this.frame.setTitle("Belsoft Snake");

        if (this.modo == 2) {//si se va a iniciar o continuar el modo campaña
            this.numVitCam = 0;//no ha comido vitaminas en este laberinto
            this.laberintoActualCampana++;//cambia de laberinto hacia el siguiente
            if (this.laberintoActualCampana >= 8) {
                this.laberintoActualCampana = 0;//retorna al primero
                this.progresoCampana += 10;
                this.getLab().intervalSnake(this.getNivelActual() + (this.progresoCampana / 10));
            }

            this.lblprogresovit.setText(this.progresoCampana + "");
            this.getLab().dibujarLaberinto(this.laberintoActualCampana);
        } else {
            this.getLab().dibujarLaberinto(this.laberintoActualClasico);
        }
        this.estaJugando = true;
        this.mostrarDireccion = false;
        this.mostrarLlenura = false;
        this.comioVit = false;
        this.Bono.setXY(-1, -1);
        this.movBono = 0;
        this.salioBono = false;

        int idlabDibujar = -1;
        if (this.modo == 1) {
            idlabDibujar = this.laberintoActualClasico;
        } else if (this.modo == 2) {
            idlabDibujar = this.laberintoActualCampana;
        }

        int npc = 3;//numero de partes del cuerpo para empezar
        NodoCuerpo nc = new NodoCuerpo(0, 0, 2);//creo un nodo que apunta hacia la derecha
        switch (idlabDibujar) {
            case 0:
            case 1:
            case 2:
            case 4:
            case 5:
            case 11:
                nc.x = 1;
                nc.y = this.getLab().getScaleHeight() / 2;
                break;
            case 3:
                nc.x = 8;
                nc.y = 4;
                break;
            case 6:
                nc.x = 12;
                nc.y = 3;
                break;
            case 7:
                nc.x = 15;
                nc.y = 0;
                break;
            case 8:
                nc.x = 0;
                nc.y = 10;
                break;
            case 9:
                nc.x = 0;
                nc.y = 5;
                break;
            case 10:
                nc.x = 0;
                nc.y = 3;
                break;
        }
        getSnake().Cabeza(nc);
        for (int i = 1; i < npc; i++) {
            this.comioVit = true;
            getSnake().mover_y_o_crecer();
        }
        this.getLab().dibujarSnake();
        lblpuntaje.setText(this.puntaje + "");
        lblpuntaje.setVisible(true);
        this.getLab().randomVit_o_bono((byte) 1);
    }

    public void keyPressed(java.awt.event.KeyEvent evt) {

        if (estaJugando == false) {
            return;
        }
        NodoCuerpo nc = this.getLab().getBody().getCabeza();
        int dir = nc.dir;
        switch (evt.getKeyCode()) {
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_UP:
            case KeyEvent.VK_DOWN:
                if (puedeMoverla || esperando) {
                    temporizador.stop();
                    puedePausar = false;
                    puedeMoverla = false;

                    nc.chDirAbsLegal(41 - evt.getKeyCode());
                    if (lblep.isVisible()) {
                        lblep.setVisible(false);
                    }
                    if (estaDetenido) {
                        frame.setTitle("Belsoft Snake");
                        estaDetenido = false;
                        movsnake.start();
                    }
                    if (dir != nc.dir) {
                        mostrarDireccion = true;
                    }
                    if (esperando) {
                        esperando = false;
                        movsnake.start();
                    }
                } else {
                    proximoMov = evt.getKeyCode();
                }
                break;

            case KeyEvent.VK_ENTER:
                if (puedePausar || esperando) {
                    temporizador.stop();
                    puedePausar = false;
                    estaDetenido = true;
                    frame.setTitle("Belsoft Snake (Pausado)");
                    movsnake.stop();
                } else if (estaDetenido == false) {
                    proximoPausar = true;
                }
        }
    }

    synchronized public void MovSnake(ActionEvent e) {
        getLab().repaint();
        ////////////////
        getLab().secuenciaMov();
        //***********
        if (this.salioBono) {
            this.movBono--;
            lblbono.setText(this.movBono + "");
            if (this.movBono == 0) {
                this.numVit = 0;
                this.getLab().borrar(this.Bono);
                this.Bono.setXY(-1, -1);
                this.salioBono = false;
                lblbono.setVisible(false);
            }
        } else if (!lblbono.isVisible()) {
            lblbono.setVisible(false);
        }
//*******************
        //comioVit=false;
        NodoCuerpo uc = getSnake().getCola(); //Captura las coordenas y dirección de la cola
        //***********
        getSnake().mover_y_o_crecer();
        //*********
        this.getLab().mostrarDireccion();//Llama al procedimiento que muestra los cruces y dirección de los puntos de la culebra
        /////
        ///---------
        if (getSnake().enCuadroOcupado()) {//se comprueba que la cabeza no esté en un lugar ocupado
            if (getSnake().getCabeza().igualPosicion(this.Vit)) {
                //sonido que comio
                this.comioVit = true;
                this.mostrarLlenura = true;
//                this.getLab().dibujarCabeza(24);
                this.getLab().dibujarCabeza();

                this.puntaje += this.getNivelActual();
                lblpuntaje.setText(this.puntaje + "");
                this.numVit++;
                if (this.modo == 2) {//campaña
                    this.numVitCam++;
                    if (this.numVitCam == this.progresoCampana) {
                        this.numVit = 1;
                        movsnake.stop();
                        this.proximoMov = 0;
                        iniciar(2);
                        return;
                    } else {
                        lblprogresovit.setText(this.progresoCampana - this.numVitCam + "");
                    }
                }

                if (this.getLab().getEspaciosNoOcupados() - 1 == 0) {
                    //DETENER TIMER DE VITAMINA
                    //HACER GOLPE
                    this.getLab().golpe();
                    return;
                } else {
                    this.getLab().randomVit_o_bono((byte) 1);
                }
                if (this.numVit >= 5) {
                    this.getLab().randomVit_o_bono((byte) 2);
                    this.salioBono = true;
                    this.movBono = (byte) (this.getLab().getScaleWidth() - 1);
                    lblbono.setText(20 + "");
                    lblbono.setVisible(true);
                    this.numVit = 0;
                }

            } else if (getSnake().getCabeza().igualPosicion(this.Bono)) {
                this.numVit = 0;
                this.mostrarLlenura = true;
//                this.getLab().dibujarCabeza(24);
                this.getLab().dibujarCabeza();
                this.puntaje += 5 + (5 * this.getNivelActual()) + (2 * this.movBono);
                lblpuntaje.setText(this.puntaje + "");
                this.salioBono = false;
                this.movBono = 0;
                this.Bono.setXY(-1, -1);
                lblbono.setVisible(false);
            } else {
                //sino fue porque se encontró con un ladrillo o con ella misma
                this.getLab().golpe();
                return;
            }

        } //else {//la coordenada de cabeza no está ocupada
        NodoCuerpo proxcabeza = getSnake().getCabeza().clone();//copia de la cabeza
        this.getLab().siguientepos(proxcabeza);//siguiente posicion a la copia de la cabeza
        if (proxcabeza.igualPosicion(this.Bono)) {//Si en el siguiente movimiento se encontrara con un bono
            //  comera=true;
            //      this.getLab().dibujarCabeza(28);
            this.getLab().dibujarCabeza();
        } else if (proxcabeza.igualPosicion(this.Vit)) {//Si en el siguiente movimiento se encontrara con un bono
            //  comera=true;
            //    this.getLab().dibujarCabeza(28);
            this.getLab().dibujarCabeza();
        } else {
            this.getLab().dibujarCabeza();
            if (!(snake.getCola().igualPosicion(proxcabeza)) && this.getLab().getCuadrocupado(proxcabeza)) {
                movsnake.stop();
                this.puedeMoverla = true;
                this.puedePausar = true;
                this.esperando = true;
                temporizador.start();
                if (buscarAutomatico) {
                    chdirfull();
                }
                return;
            }
        }
        //}
        if (this.proximoPausar) {
            this.proximoPausar = false;
            keyPressed(new KeyEvent(lblep, 0, (long) 0, 0, KeyEvent.VK_ENTER, (char) 0));
        } else if (this.proximoMov != 0) {
            keyPressed(new KeyEvent(lblep, 0, (long) 0, 0, this.proximoMov, (char) 0));
            this.proximoMov = 0;
        }
        this.puedeMoverla = true;
        this.puedePausar = true;
        if (buscarAutomatico) {
            chdirfull();
        }
        //comioVit=false;

    }

    public void detener() {
        if (this.estaJugando) {
            frame.setTitle("Belsoft Snake(Juego Terminado)");
            movsnake.stop();
            temporizador.stop();
            this.estaJugando = false;
        }
    }

    public void invalidarEstrellada() {
        this.estaJugando = true;
        movsnake.start();
    }

    private void chdirfull() {
        codigo.ListaInteligencia li = new ListaInteligencia(this.getSnake().getCabeza().clone(), this);//creo la lista
        //de inteligencia que cuenta cuantos cuadros hay en cada una de las direcciones

        li.dirInteligencia();
        //   if (li.numdirInteligencia==1){
        //    System.out.println("Una dirección");
        //  keyPressed(new KeyEvent(lblbono, 0, (long) 0, 0, 41 - li.dirInteligencia[0], (char) 0));
        //}else{
        int i = 0;
        if (salioBono) {
            i = ia.getDir(snake.getCabeza().clone(), Bono, snake.getCabeza().clone().getDirRel(2), snake.getCabeza().dir, false);
            if (ia.getNumRecorrido() >= movBono) {
                i = ia.getDir(snake.getCabeza().clone(), Vit, snake.getCabeza().clone().getDirRel(2), snake.getCabeza().dir, false);
            }
        } else {
            //se busca una direccion que vaya desde la cabeza a la vitamina
            i = ia.getDir(snake.getCabeza().clone(), Vit, snake.getCabeza().clone().getDirRel(2), snake.getCabeza().dir, false);
            if (i == 0) {//si no es posible llegar a la vitamina
                //jeje, intentaré llegar a la cola;

                i = ia.getDir(snake.getCabeza().clone(), snake.getCola().clone(), snake.getCabeza().clone().getDirRel(2), snake.getCabeza().dir, false);
            }
        }

        if (i != 0) {
            //System.out.println("I="+i);

            if (li.estaDirenInteligencia(i)) {
                keyPressed(new KeyEvent(lblbono, 0, (long) 0, 0, 41 - i, (char) 0));
            } else {
                int ran = (int) ((Math.random() * li.numdirInteligencia));
                keyPressed(new KeyEvent(lblbono, 0, (long) 0, 0, 41 - li.dirInteligencia[ran], (char) 0));
            }
            lblpuntaje.setText(ia.getNumCruces() + "");
        } else {
            // System.out.println("I=0");
            if (!li.estaDirenInteligencia(this.snake.getCabeza().dir)) {
                int ran = (int) ((Math.random() * li.numdirInteligencia));
                keyPressed(new KeyEvent(lblbono, 0, (long) 0, 0, 41 - li.dirInteligencia[ran], (char) 0));
                //       System.out.print(" random");
            }
        }

        //}
    }

    javax.swing.Timer movVit = new javax.swing.Timer(50, new ActionListener() {

        public void actionPerformed(ActionEvent e) {
            moviVit++;
            if (moviVit == 5) {
                moviVit = 1;
            }
            lab.dibujarVit();
        }
    });

    javax.swing.Timer temporizador = new javax.swing.Timer(200, new ActionListener() {

        public void actionPerformed(ActionEvent e) {
            esperando = false;
            movsnake.start();
        }
    });
    javax.swing.Timer movsnake = new javax.swing.Timer(20, new ActionListener() {

        public void actionPerformed(ActionEvent e) {
            try {
                MovSnake(e);
            } catch (Exception ex) {

                System.out.println("Error raro");
                ex.printStackTrace();
            }

        }
    });
    javax.swing.Timer busqueda = new javax.swing.Timer(100, new ActionListener() {

        public void actionPerformed(ActionEvent e) {

            int dir = ia.getDir(origen, destino);
            origen.dir = dir;
            //System.out.println("probando "+dir);
            //System.out.println("dir"+dir);
            ia.siguientepos(origen);
            getLab().dibujarOrigenR(origen.x, origen.y);

//            dir=ia.getDir(destino, snake.getXY(1).clone());
            //          destino.dir=dir;
            //        ia.siguientepos(destino);
            //      this.lab.dibujarVit();
        }
    });

    javax.swing.Timer timerVit = new javax.swing.Timer(500, new ActionListener() {

        public void actionPerformed(ActionEvent e) {
            System.out.println(Vit.toString());
            getLab().borrar(Vit);

            int dir = ia.getDir(Vit, getSnake().getCabeza());
            System.out.print(" dir " + dir);
            Vit.dir = dir;
            ia.siguientepos(Vit);
            System.out.print("---" + Vit.toString());
            getLab().dibujarVit();
        }
    });

    public void setLblep(javax.swing.JLabel lblep) {
        this.lblep = lblep;
    }

    public void setLblbono(javax.swing.JLabel lblbono) {
        this.lblbono = lblbono;
    }

    public void setLblpuntaje(javax.swing.JLabel lblpuntaje) {
        this.lblpuntaje = lblpuntaje;
    }

    public void setLblprogresovit(javax.swing.JLabel lblprogresovit) {
        this.lblprogresovit = lblprogresovit;
    }

    public void setPanelJuego(javax.swing.JPanel panelJuego) {
        this.panelJuego = panelJuego;
    }

    public void setFrame(javax.swing.JFrame frame) {
        this.frame = frame;
    }

    public void presentacion() {
        setLab(new Laberinto(panelJuego, 22, 12, null));//creo un laberinto
        getLab().setWorld(this);
//        world1.lab.dibujarLaberinto(1);
        getLab().dibujarBs();
    }

    public Laberinto getLab() {
        return lab;
    }

    public void setLab(Laberinto lab) {
        this.lab = lab;
        ia = new IAtree(lab);
    }

    public byte getNivelActual() {
        return nivelActual;
    }

    public BodySnake getSnake() {
        return snake;
    }

    public byte getMovCabeza() {
        return movCabeza;
    }

    public byte getMovCuerpo() {
        return movCuerpo;
    }

    public byte getMovCola() {
        return movCola;
    }

    public void activarBusqueda() {
        busqueda.start();
    }

    public void detenerBusqueda() {
        busqueda.stop();
    }
}
