package codigo;
/**
 * @author BS
 */

//esta clase es la otra parte de la inteligencia de la culebra...
//trabaja de la siguiente forma....
//desde un bloque de inicio busca la forma de desplazarse a un bloque de fin mediante
//movimientos lineales
//cuando no puede desplazarse debido a que hay un bloqueo en la ruta que impide
//llegar al bloque final, entonces esta clase se refuerza de la clase LISTAINTELIGENCIA\
//para obtener el dato de la mejor direccion a elegir debido al mayor numero de espacios

//Esta clase todavia esta en construccion, debe utilizar algo mas de Inteligencia para
//tener movimientos con pensamiento mas humano

public class IAtree {
    private Laberinto lab;//Laberinto para relacionar los espacios ocupados
    private NodoIaTree nprincipal=null;//Raiz principal
    private boolean ocupado[][];//Ocupacion temporal de espacios

    private ListaIA cabezas=null;//Expansion de cabezas

    private int numRecorrido=0;//Numero de espacios recorridos

    private int numCruces=0;//Numero de cruces para llegar al destino
    public IAtree(Laberinto lab){
        this.lab=lab;
    }
    private void ocupados(NodoCuerpo nc, boolean v){
        ocupado[nc.x][nc.y]=v;
    }
    //encuentra posible movimiento para llegar a una posicion final
    //se verifican todas las direcciones, empezando desde la derecha
    public int getDir(NodoCuerpo origen, NodoCuerpo destino){
        return getDir(origen, destino, 0);
    }
    //se verifican las direcciones exeptuando la pasada como parametro
    //empezando desde la derecha
    public int getDir(NodoCuerpo origen, NodoCuerpo destino, int dirNoVerificar){
        return getDir(origen, destino, dirNoVerificar, 1,false);
    }
    //esta es una modificacion del proceso, se hace un posible movimiento inteligente
    //teniendo en cuenta la anterior posicion del origen actual, especificada como dorigen
    public int getDir(NodoCuerpo dorigen, NodoCuerpo origen, NodoCuerpo destino, int dirNoVerificar, int dirPrincipal){
        return 0;
    }
    //es la funcion clave de la clase y es donde se da la parte logica de la busqueda
    //no se verifica una direccion, se tiene una direccion principal de busqueda
    //e indicamos si queremos evaluar direcciones aparte de la principal...
    public int getDir(NodoCuerpo origen, NodoCuerpo destino, int dirNoVerificar, int dirPrincipal, boolean soloDirPrincipal){
        numRecorrido=0;
        int dir=0;//Se establece el No movimiento para el caso en que no se encuentre direccion
        ocupado=new boolean[this.lab.getScaleWidth()][this.lab.getScaleHeight()];//Redimensiona
        //los espacios ocupados de acuerdo a la cantidad de espacios
        cabezas=new ListaIA();//creo la lista que contendrá las cabezas

        //la Variable nprincipal no es utilizada en un lugar aparte, pero es necesaria
        //si se quiere saber el arbol de caminos
        nprincipal=new NodoIaTree(origen);//convierto el Nodo Origen en un Nodo Arbol
        nprincipal.dir=0;//no le coloco direccion alguna al nodo inicial
        ocupados(origen, true);//ocupo el espacio del origen

        NodoIaTree raiz=null;//esta es una copia de la raiz
        int i=dirPrincipal;
        int r=0;//recorridos en el siguiente mientras
        while(i<=4){
            r++;
//        for ( i = 1; i <=4; i++) {
            if (i!=dirNoVerificar){//solo verifico si no corresponde a la direccion que no quiero verificar
                raiz=new NodoIaTree(origen);//convierto el Nodo Origen en un Nodo Arbol
                raiz.dir=i;//le coloco cada una de las direcciones al nodo Inicial;
                siguientepos(raiz);//obtengo cada una de las posiciones del nodo Inicial, esto se da mediante el ciclo

                raiz.setDirInicial(i);//coloco cual fue la direccion inicial cogida
                if (raiz.igualPosicion(destino)){//Verificamos la posicion tomada
                    numRecorrido=1;
                    return i;
                }else if (!this.lab.getCuadrocupado(raiz)){//si no está ocupado el espacio de direccion lo inserto en una lista
                    //de posibles cabezas
                    nprincipal.setDirInicial(i);
                    nprincipal.setPosdir(i, raiz);//desencadeno la primera rama
                    cabezas.insertar(raiz);//inserto el nodo en la lista de cabezas
//                    System.out.println("dir"+tmpnia.dir+ " nr"+tmpnia.nr);
                    ocupados(raiz, true);//ocupo el espacio
                }                
            }
            if (r==1){//si es la primera vez que entra en el ciclo
                if (i!=1)i=1;//y la direccion principal no es la primera, entonces coloca la primera
                else i++;
            }else{
                i++;
                if (i==dirPrincipal)i++;
            }
//        }
            if (soloDirPrincipal)break;//si solo quiero evaluar la direccion principal, no continuo
            //el ciclo de verificacion de las demas direcciones
        }
        while(cabezas.getPtr()!=null){//Mientras haya cabezas que evaluar con direccion
            NodoIaTree tmptr=cabezas.getPtr();//temporal para recorrer la lista
            ListaIA tmpcabezas=new ListaIA();//Creo una nueva lista en este ciclo
//            System.out.println("Cambiando cabeza");

            while(tmptr!=null){//Recorro la lista de cabezas
                //System.out.println("numero de recorridos :"+tmptr.nr);
                for (i = 1; i <= 4; i++) {//Evaluamos las cuatro direcciones
                    if (i!=(tmptr.getDirRel(2))){//Si la direccion a evaluar no es la contraria a la que lleva
  //                      System.out.println("Dir" + i);
                        NodoIaTree nuevarama=new NodoIaTree(tmptr);//Crea una rama con la posicion y direccion que la raiz
                        nuevarama.dir=i;//le coloco la direccion a la rama
                        siguientepos(nuevarama);//le coloca la siguiente posicion a la rama

                        if (nuevarama.igualPosicion(destino)){
                                tmptr.setPosdir(i, nuevarama);//Agrega la rama al temporal de cabeza

                    //            System.out.println("Ubicado en " + nuevarama.nr+ ".."+nuevarama.getDirInicial()+".."+nuevarama.toString());
                                numRecorrido=nuevarama.nr;
                                numCruces=nuevarama.getNumCruces();
                                return nuevarama.getDirInicial();
                        }
                        else if (!(this.lab.getCuadrocupado(nuevarama) || this.ocupado[nuevarama.x][nuevarama.y] 
                                || (this.lab.getBody().estaEnCulebra(nuevarama.nr, nuevarama)) )){//si no está ocupado el espacio en el laberinto
                            //if (!tmptr.estaenrecorrido(nuevarama)){//Si no se ha añadido esta rama en el recorrido
                            tmptr.setPosdir(i, nuevarama);
                            this.ocupados(nuevarama, true);//ocupo el espacio
                                tmpcabezas.insertar(nuevarama);//va insertando en la nueva lista de cabezas las actuales direcciones
                            //}
                        }
                    }
                }
                tmptr=tmptr.getSig();
            }
            cabezas=tmpcabezas;
        }
        return dir;
       // while (cabezas.getTamano()!=0){//Va a realizar esto mientras el número de cabezas NO sea igual a cero, es decir, todas las rutas
            //encuentren una terminación o en un caso interno si doy un break, porque ya no necesito evaluar mas.
        //}
    }

    public void siguientepos(NodoCuerpo nc) {
        switch (nc.dir) {
            case 1:nc.y++;if (nc.y >= lab.getScaleHeight()) {nc.y = 0;}break;
            case 2:nc.x++;if (nc.x >= lab.getScaleWidth()) {nc.x = 0;}break;
            case 3:nc.y--;if (nc.y < 0) {nc.y = lab.getScaleHeight() - 1;}break;
            case 4:nc.x--;if (nc.x < 0) {nc.x = lab.getScaleWidth() - 1;}break;
        }
    }
    public void dibujarAmplitud(){
        NodoIaTree tmpr=cabezas.getPtr();
        System.out.println("dibujar");
            System.out.println("Lista len"+cabezas.getTamano());
        while(tmpr!=null){
            this.lab.dibujarAmplitud(tmpr);

            //System.out.println(tmpr.x +":"+tmpr.y+"-"+tmpr.dir);
            tmpr=tmpr.getSig();
        }
    }

    public int getNumRecorrido() {
        return numRecorrido;
    }

    public int getNumCruces() {
        return numCruces;
    }
}
class ListaIA {
    NodoIaTree ptr;
    NodoIaTree fin;
    private int tamano=0;

    public void insertar(NodoIaTree obj){
        if (ptr==null){
            ptr=obj;
            fin=obj;
        }else{
            fin.setSig(obj);
            fin=fin.getSig();
        }
        tamano++;
    }
    public void eliminarCabeza(){
        if (ptr!=null){
            ptr=ptr.getSig();
            if (ptr==null)fin=null;
        }
    }
    public void eliminarCola(){
        if (ptr!=null){
            fin=fin.getAnt();
            fin.setSig(null);
        }
    }
    public NodoIaTree getPtr(){
        return ptr;
    }
    public void setPtr(NodoIaTree ptr){
        this.ptr=ptr;
        if (ptr!=null)ptr.setAnt(null);
    }
    public NodoIaTree getFin(){
        return fin;
    }
    public void setFin(NodoIaTree fin){
        this.fin=fin;
        if (fin.getSig()!=null)fin.setSig(null);
    }
    public NodoIaTree getIaTree(int pos){
        NodoIaTree ret=null;
        if (ptr!=null){
            if (ptr.getSig()==null){
                return ptr;
            }else{
                NodoIaTree tmptr=ptr;
                int rec=0;
                do{
                    rec++;
                    if (rec==pos){
                        return tmptr;
                    }
                    tmptr=tmptr.getSig();
                }while(tmptr!=null);
            }
        }
        return ret;
    }
    public int getTamano() {
        return tamano;
    }
    public void vaciar(){
        ptr=null;
        fin=null;
        this.tamano=0;
    }

}