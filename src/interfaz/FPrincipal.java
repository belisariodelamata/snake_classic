/*
 * FPrincipal.java
 *
 * Created on 11-abr-2010, 8:30:44
 */
package interfaz;

import codigo.BodySnake;
import codigo.Laberinto;
import codigo.Game;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import codigo.IAtree;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * 0
 *
 * @author BS
 */
public class FPrincipal extends javax.swing.JFrame {

    Game world1 = new Game();
    BodySnake snake;
    BodySnake snake2;

    codigo.IAtree ia = null;

    public FPrincipal() {
        initComponents();
        initComponents2();
//        setExtendedState(MAXIMIZED_BOTH);

        cargarMenuLaberintos();
        cargarMenuNiveles();
        cargarMenuModos();
    }

    @Override
    public void paint(Graphics g) {
        try {
            super.paint(g);
            if (world1.getLab() != null) {
                world1.getLab().repaint();
            }
        } catch (Exception e) {

        }
    }

    private void initComponents2() {
        panelJuego = new BsJpanel();
        panelJuego.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelJuego.setBackground(new java.awt.Color(255, 255, 255));
        panelJuego.world1 = this.world1;
        panelJuego.addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            @SuppressWarnings("empty-statement")
            public void mousePressed(java.awt.event.MouseEvent evt) {
                if (checkdestino.isSelected()) {
                    world1.getLab().dibujarDestino(evt.getX(), evt.getY());
                    world1.destino = world1.getLab().crearBloque(evt.getX(), evt.getY());
                } else if (checkorigen.isSelected()) {
                    world1.getLab().dibujarOrigen(evt.getX(), evt.getY());
                    world1.origen = world1.getLab().crearBloque(evt.getX(), evt.getY());
                } else if (checkdibujar.isSelected()) {
                    if (evt.getButton() == 1) {
                        try {
                            world1.getLab().drawBlock(evt.getX(), evt.getY());
                        } catch (Exception e) {
                            e.printStackTrace();
                        };

                    } else {
                        world1.getLab().borrarReal(evt.getX(), evt.getY());
                    }

                } else {

                    codigo.ListaInteligencia li = new codigo.ListaInteligencia(world1.getLab().crearBloque(evt.getX(), evt.getY()), world1);
                    System.out.println("-");
                    for (int i = 1; i < 5; i++) {

                        System.out.println(li.blancosenDir(i));

                    }
                }

                //System.out.println (li.bloquesAlrededor(world1.lab.crearBloque(evt.getX(), evt.getY())));
            }
        });

        panelJuego.setLayout(null);

        getContentPane().add(panelJuego);
        panelJuego.setBounds(this.getWidth() / 2 - (840 / 2), 50, 840, this.getHeight() - 150);
        panelJuego.setDoubleBuffered(true);
        world1.setFrame(this);
        world1.setLblbono(lblbono);
        world1.setLblep(lblprogresovit);
        world1.setLblprogresovit(lblprogresovit);
        world1.setLblpuntaje(lblpuntaje);
        world1.setPanelJuego(panelJuego);

    }

    private void cargarMenuModos() {
        javax.swing.JCheckBoxMenuItem modos;
        for (int x = 1; x <= 2; x++) {
            modos = new javax.swing.JCheckBoxMenuItem();
            modos.setName(x + "");
            if (x == 1) {
                modos.setText("Clásico");

            } else if (x == 2) {
                modos.setText("Campaña");
            }

            if (x == world1.modo) {
                modos.setState(true);
                modoAnt = modos;
            }
            MenuModo.add(modos);
            modos.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    menuModosClick(e);
                }
            });
        }
    }
    javax.swing.JCheckBoxMenuItem modoAnt = null;//Nivel anterior

    private void menuModosClick(ActionEvent e) {
        javax.swing.JCheckBoxMenuItem menu = (javax.swing.JCheckBoxMenuItem) e.getSource();
        int mod = Integer.parseInt(menu.getName());
        if (mod == world1.modo) {
            modoAnt.setState(true);
            return;
        }
        if (modoAnt != null) {
            modoAnt.setState(false);
        }
        world1.modo = (byte) mod;
        modoAnt = menu;
        menu.setState(true);
        if (mod == 1) {
            menuLaberintos.setVisible(true);
            menuNivelClick(new ActionEvent(menuniveles.getObject(world1.nivelesClasico), world1.nivelesClasico, null));
        } else if (mod == 2) {
            menuLaberintos.setVisible(false);
            menuNivelClick(new ActionEvent(menuniveles.getObject(world1.nivelesCampana), world1.nivelesCampana, null));
        }
        world1.detener();
    }

    extras.ListaObjeto menuniveles = new extras.ListaObjeto();

    private void cargarMenuNiveles() {
        javax.swing.JCheckBoxMenuItem niveles;
        for (int x = 1; x <= 14; x++) {
            niveles = new javax.swing.JCheckBoxMenuItem(x + "");
            niveles.setName(x + "");
            menuNivel.add(niveles);
            menuniveles.insertar(niveles);
            if (x == world1.getNivelActual()) {
                nivelAnt = niveles;
                niveles.setState(true);
            }
            niveles.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    menuNivelClick(e);
                }
            });
        }
    }
    private javax.swing.JCheckBoxMenuItem nivelAnt = null;//Nivel anterior

    private void menuNivelClick(ActionEvent e) {
        javax.swing.JCheckBoxMenuItem menu = (javax.swing.JCheckBoxMenuItem) e.getSource();
        int niv = Integer.parseInt(menu.getName());
        if (niv == world1.getNivelActual()) {
            nivelAnt.setState(true);
            return;
        }
        if (nivelAnt != null) {
            nivelAnt.setState(false);
        }
        nivelAnt = menu;

        if (world1.modo == 1) {
            world1.nivelesClasico = (byte) niv;
        } else if (world1.modo == 2) {
            world1.nivelesCampana = (byte) niv;
        }
        world1.nivelActual = (byte) niv;
        menu.setState(true);

        world1.detener();
    }

    private void cargarMenuLaberintos() {
        javax.swing.JCheckBoxMenuItem laberintos;
        for (int x = 0; x <= 11; x++) {
            laberintos = new javax.swing.JCheckBoxMenuItem();
            if (x == 0) {
                laberintos.setText("Sin Laberinto");
            } else {
                laberintos.setText(x + "");
            }
            laberintos.setName(x + "");
            if (x == world1.laberintoActualCampana) {
                laberintos.setState(true);
                labAnt = laberintos;
            }
            menuLaberintos.add(laberintos);
            laberintos.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    laberintosClick(e);
                }
            });
        }
    }
    private javax.swing.JCheckBoxMenuItem labAnt = null;//laberinto anterior

    private void laberintosClick(ActionEvent e) {
        world1.detener();
        javax.swing.JCheckBoxMenuItem menu = (javax.swing.JCheckBoxMenuItem) e.getSource();
        int indlab = Integer.parseInt(menu.getName());
        if (indlab == world1.laberintoActualClasico) {
            labAnt.setState(true);
            return;
        }
        if (labAnt != null) {
            labAnt.setState(false);
        }
        labAnt = menu;
        world1.setLab(new Laberinto(panelJuego, 22, 12, null));//creo un laberinto
        world1.getLab().dibujarLaberinto(indlab);
        menu.setState(true);
        world1.laberintoActualClasico = Byte.parseByte(menu.getName());
        world1.detener();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lblprogresovit = new javax.swing.JLabel();
        lblpuntaje = new javax.swing.JLabel();
        lblbono = new javax.swing.JLabel();
        checkdibujar = new javax.swing.JCheckBox();
        jButton2 = new javax.swing.JButton();
        checkdestino = new javax.swing.JCheckBox();
        checkorigen = new javax.swing.JCheckBox();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        onoff = new javax.swing.JToggleButton();
        jLabel2 = new javax.swing.JLabel();
        onoff1 = new javax.swing.JToggleButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        bsMenu = new javax.swing.JMenu();
        menuNuevoJuego = new javax.swing.JMenuItem();
        MenuModo = new javax.swing.JMenu();
        menuNivel = new javax.swing.JMenu();
        menuLaberintos = new javax.swing.JMenu();
        menuPuntajeMax = new javax.swing.JMenuItem();
        mnuBusquedaAutomatica = new javax.swing.JCheckBoxMenuItem();
        mnuCodigoLaberinto = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Belsoft Caterpillar");
        setBackground(new java.awt.Color(0, 0, 0));
        setForeground(new java.awt.Color(255, 255, 255));
        setResizable(false);
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                formMouseMoved(evt);
            }
        });
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });
        getContentPane().setLayout(null);

        jButton1.setText("Generar Codigo");
        jButton1.setVisible(false);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(910, 10, 100, 23);
        getContentPane().add(jLabel1);
        jLabel1.setBounds(880, 60, 130, 120);

        lblprogresovit.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        lblprogresovit.setForeground(new java.awt.Color(0, 0, 255));
        lblprogresovit.setText("progreso vitamina");
        getContentPane().add(lblprogresovit);
        lblprogresovit.setBounds(520, 10, 230, 30);

        lblpuntaje.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        lblpuntaje.setForeground(new java.awt.Color(255, 0, 51));
        lblpuntaje.setText("etiqueta puntaje");
        lblpuntaje.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblpuntajeMouseClicked(evt);
            }
        });
        getContentPane().add(lblpuntaje);
        lblpuntaje.setBounds(30, 10, 210, 29);

        lblbono.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        lblbono.setForeground(new java.awt.Color(0, 0, 204));
        lblbono.setText("Bono");
        getContentPane().add(lblbono);
        lblbono.setBounds(360, 10, 190, 30);

        checkdibujar.setSelected(true);
        checkdibujar.setText("dibujar");
        getContentPane().add(checkdibujar);
        checkdibujar.setBounds(880, 30, 59, 23);
        checkdibujar.setVisible(false);

        jButton2.setText("Crear Ia");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(910, 160, 73, 23);
        jButton2.setVisible(false);

        checkdestino.setText("destino");
        getContentPane().add(checkdestino);
        checkdestino.setBounds(900, 220, 61, 23);
        checkdestino.setVisible(false);

        checkorigen.setText("origen");
        getContentPane().add(checkorigen);
        checkorigen.setBounds(900, 250, 81, 23);
        checkorigen.setVisible(false);

        jButton3.setText("Posible Mov");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton3);
        jButton3.setBounds(910, 190, 89, 23);
        jButton3.setVisible(false);

        jButton4.setText("Arbol");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton4);
        jButton4.setBounds(893, 320, 100, 23);
        jButton4.setVisible(false);

        onoff.setText("OFF");
        onoff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onoffActionPerformed(evt);
            }
        });
        getContentPane().add(onoff);
        onoff.setBounds(900, 370, 100, 23);
        onoff.setVisible(false);

        jLabel2.setText("Movimiento Vitamina");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(900, 510, 110, 14);
        jLabel2.setVisible(false);

        onoff1.setText("OFF");
        onoff1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onoff1ActionPerformed(evt);
            }
        });
        getContentPane().add(onoff1);
        onoff1.setBounds(900, 540, 100, 23);
        onoff1.setVisible(false);

        bsMenu.setText("Juego");
        bsMenu.addMenuListener(new javax.swing.event.MenuListener() {
            public void menuCanceled(javax.swing.event.MenuEvent evt) {
            }
            public void menuDeselected(javax.swing.event.MenuEvent evt) {
            }
            public void menuSelected(javax.swing.event.MenuEvent evt) {
                bsMenuMenuSelected(evt);
            }
        });
        bsMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bsMenuActionPerformed(evt);
            }
        });

        menuNuevoJuego.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        menuNuevoJuego.setText("Juego Nuevo");
        menuNuevoJuego.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuNuevoJuegoActionPerformed(evt);
            }
        });
        bsMenu.add(menuNuevoJuego);

        MenuModo.setText("Modo");
        bsMenu.add(MenuModo);

        menuNivel.setText("Nivel");
        bsMenu.add(menuNivel);

        menuLaberintos.setText("Laberintos");
        bsMenu.add(menuLaberintos);

        menuPuntajeMax.setText("Puntaje Máximo");
        menuPuntajeMax.setSelected(true);
        menuPuntajeMax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPuntajeMaxActionPerformed(evt);
            }
        });
        bsMenu.add(menuPuntajeMax);

        mnuBusquedaAutomatica.setText("Búsqueda Automática");
        mnuBusquedaAutomatica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuBusquedaAutomaticaActionPerformed(evt);
            }
        });
        bsMenu.add(mnuBusquedaAutomatica);

        mnuCodigoLaberinto.setText("Generar Código de Laberinto");
        mnuCodigoLaberinto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuCodigoLaberintoActionPerformed(evt);
            }
        });
        bsMenu.add(mnuCodigoLaberinto);

        jMenuBar1.add(bsMenu);

        setJMenuBar(jMenuBar1);

        setSize(new java.awt.Dimension(1043, 776));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void menuNuevoJuegoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuNuevoJuegoActionPerformed
        world1.iniciar(1);
    }//GEN-LAST:event_menuNuevoJuegoActionPerformed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        this.world1.keyPressed(evt);
    }//GEN-LAST:event_formKeyPressed
    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
    }//GEN-LAST:event_formMouseClicked

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        JOptionPane.showMessageDialog(rootPane, "Belsoft Inmaginations", "Bs/Record", JOptionPane.INFORMATION_MESSAGE);
        world1.presentacion();
    }//GEN-LAST:event_formWindowOpened

    private void menuPuntajeMaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPuntajeMaxActionPerformed
        world1.getLab().borrarTodo();
    }//GEN-LAST:event_menuPuntajeMaxActionPerformed

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        if (world1.getLab() != null) {
            world1.getLab().repaint();
        }
    }//GEN-LAST:event_formWindowActivated

    private void formMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseMoved
        if (world1.getLab() != null) {
            world1.getLab().repaint();
        }
    }//GEN-LAST:event_formMouseMoved

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        formKeyPressed(evt);

    }//GEN-LAST:event_jButton1KeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.world1.getLab().codigoLaberinto();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void bsMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bsMenuActionPerformed
        System.out.println("jkjk");
    }//GEN-LAST:event_bsMenuActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        ia = new IAtree(world1.getLab());
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        ia.dibujarAmplitud();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void onoffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onoffActionPerformed
        if (onoff.isSelected()) {
            world1.activarBusqueda();
            onoff.setText("On");
        } else {
            world1.detenerBusqueda();
            onoff.setText("Off");
        }

    }//GEN-LAST:event_onoffActionPerformed

    private void onoff1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onoff1ActionPerformed
        /*if (onoff1.isSelected()){
        timerVit.start();
        onoff1.setText("On");
        }else{
        timerVit.stop();
        onoff1.setText("Off");
        }*/
    }//GEN-LAST:event_onoff1ActionPerformed

    private void lblpuntajeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblpuntajeMouseClicked
        world1.invalidarEstrellada();
    }//GEN-LAST:event_lblpuntajeMouseClicked

    private void bsMenuMenuSelected(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_bsMenuMenuSelected
        world1.detener();
    }//GEN-LAST:event_bsMenuMenuSelected

    private void mnuBusquedaAutomaticaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuBusquedaAutomaticaActionPerformed
        world1.buscarAutomatico = mnuBusquedaAutomatica.isSelected();
    }//GEN-LAST:event_mnuBusquedaAutomaticaActionPerformed

    private void mnuCodigoLaberintoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuCodigoLaberintoActionPerformed
        String codigoJava = this.world1.getLab().codigoLaberinto();
        JOptionPane.showMessageDialog(rootPane, codigoJava);
    }//GEN-LAST:event_mnuCodigoLaberintoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        javax.swing.JFrame.setDefaultLookAndFeelDecorated(true);
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new FPrincipal().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu MenuModo;
    private javax.swing.JMenu bsMenu;
    private javax.swing.JCheckBox checkdestino;
    private javax.swing.JCheckBox checkdibujar;
    private javax.swing.JCheckBox checkorigen;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JLabel lblbono;
    private javax.swing.JLabel lblprogresovit;
    private javax.swing.JLabel lblpuntaje;
    private javax.swing.JMenu menuLaberintos;
    private javax.swing.JMenu menuNivel;
    private javax.swing.JMenuItem menuNuevoJuego;
    private javax.swing.JMenuItem menuPuntajeMax;
    private javax.swing.JCheckBoxMenuItem mnuBusquedaAutomatica;
    private javax.swing.JMenuItem mnuCodigoLaberinto;
    private javax.swing.JToggleButton onoff;
    private javax.swing.JToggleButton onoff1;
    // End of variables declaration//GEN-END:variables
    private BsJpanel panelJuego;
}

class BsJpanel extends JPanel {

    public Game world1 = null;
}
